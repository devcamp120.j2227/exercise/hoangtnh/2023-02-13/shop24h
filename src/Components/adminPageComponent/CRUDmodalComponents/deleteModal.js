import { useDispatch, useSelector } from "react-redux"
import { Dialog, Typography, Grid, DialogTitle, DialogContent, DialogActions, Button } from "@mui/material";
import { confirmDeleteOrderHandler, setStatusDelete } from "../../../actions/account.action";
import CustomizedSnackbars from "./toast";
export const DeleteModal = () =>{
    const dispatch = useDispatch();
    const {orderDelete, statusDelete, toast} = useSelector((reduxData)=>
        reduxData.accountReducer
    )
    const onBtnDeleteHandler = () =>{
        dispatch(confirmDeleteOrderHandler(orderDelete._id));
    }
    const handleClose = () =>{
        dispatch(setStatusDelete(false));
    }
    return(
        <>
        {Object.keys(orderDelete).length === 0 ? null :
            <Dialog open= {statusDelete} onClose={handleClose} scroll= "paper">  
                    <DialogTitle>
                        <Typography textAlign="center" variant="h6" component="h2">
                            DELETE ORDER
                        </Typography>
                    </DialogTitle>
                    <DialogContent dividers='paper'>
                        <Typography sx ={{mt:2}}>
                            <Grid container spacing={2}>
                                <Grid item >
                                    <p>Confirm to Delete this order: {orderDelete._id.substr(orderDelete._id.length - 6)}</p>
                                </Grid>
                            </Grid>
                        </Typography>
                    </DialogContent>
                    <DialogActions>
                        <Button  onClick={handleClose} variant="outlined">Close</Button>
                        <Button onClick={onBtnDeleteHandler} variant="contained" style={{backgroundColor:"#d41616"}}>Delete Order</Button>
                    </DialogActions>
            </Dialog>
            }
            {toast.open? <CustomizedSnackbars/>: null}

        </>
    )
}