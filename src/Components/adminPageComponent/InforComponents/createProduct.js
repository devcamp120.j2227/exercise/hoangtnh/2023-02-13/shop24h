import { Link } from "react-router-dom";
import { Breadcrumb, BreadcrumbItem } from "reactstrap";
import { Grid, Typography, TextField, TextareaAutosize, Button, MenuItem } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { createNewProduct, getImgUrlHandler, getStatusDeleteImg, openToast } from "../../../actions/account.action";
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { storage } from "../../../firebase";
import { ref, uploadBytes, listAll, getDownloadURL } from "firebase/storage";
import { v4 } from "uuid"
import CustomizedSnackbars from "../CRUDmodalComponents/toast";
import AddIcon from '@mui/icons-material/Add';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { useState } from "react";
import { useEffect } from "react";
import { DeleteModalImage } from "../CRUDmodalComponents/deleteImageModal";
const CreateProduct = () =>{
    const {toast} = useSelector((reduxData)=>
        reduxData.accountReducer
    );
    const dispatch = useDispatch()

    const [productDescription , getProductDescription] = useState ("") 
    const [productName, getProductName] = useState("");
    const [productType, getProductType] = useState("");
    const [description, getDescription] = useState ("");
    const [buyPrice, getBuyPrice] = useState(0);
    const [promotionPrice, getPromotionPrice] = useState(0);
    const [imgUrl, getImgUrl] = useState(null);
    const [imgList, getImgList] = useState([]);
    const [imgSelected, getImgSelected] = useState(null)
    const [deliveryFee, getDeliveryFee] = useState("");
    const [deliverTime, getDeliverTime] = useState("");
    const [warrantyTime, getWarrantyTime] = useState("");
    const [amount, getAmount]  = useState(1)

    const newProduct = {
        Name: productName,
        Type: productType,
        Description: description,
        BuyPrice: buyPrice,
        PromotionPrice : promotionPrice,
        ProductDescription: productDescription,
        ImageUrl: imgSelected,
        DeliveryFee: deliveryFee,
        DeliverTime: deliverTime,
        Warranty: warrantyTime,
        Amount: amount
    }
    useEffect(()=>{
        //sử dụng promise.all để khắc phục lỗi duplicate
        const fetchImages = async () => {
            const result = await listAll(imgListRef);
            const urlPromises = result.items.map((imageRef) =>{
              return getDownloadURL(imageRef)
            });
      
            return Promise.all(urlPromises);
          };
      
          const loadImages = async () => {
            const urls = await fetchImages();
            getImgList(urls);
          };
          loadImages();
         
    }, [toast]);
    const imgListRef = ref(storage, "imageProduct/")

    //xử lý thông tin link ảnh
    const onImageUrlHandler = ()=>{
        if(imgUrl === null) {
            return false;
        }
        else{
            const imgRef = ref(storage, `imageProduct/${imgUrl.name + v4()}`);
            uploadBytes(imgRef, imgUrl)
                .then(() =>{
                    dispatch(openToast({
                        open: true,
                        message: "upload image successfull",
                        type: "success"
                    }))
                })
                .then(()=>{
                    getImgUrl(null)
                })
        }
    }
    //nút create product
    const onBtnSaveProduct =()=>{
        dispatch(createNewProduct(newProduct));
     }
     //xóa ảnh trên firebase
    const deleteImageHandler =(url)=>{
        dispatch(getImgUrlHandler(url));
        dispatch(getStatusDeleteImg(true))
    }

    return (
        <>
            <DeleteModalImage/>
            {toast.open? <CustomizedSnackbars/>: null}
            <Breadcrumb>
                <BreadcrumbItem >
                    <Link to="/Administrator/Staff" style={{ textDecoration: "none" }}>Products</Link>
                </BreadcrumbItem>
                <BreadcrumbItem >
                    <Link>Create product</Link>
                </BreadcrumbItem>
            </Breadcrumb>
            <Grid container direction="column"
                justifyContent="flex-start"
                alignItems="flex-start"
                style={{ backgroundColor: "whiteSmoke", padding: 20, borderRadius: "20px" }}
                md={12} sm={12} lg={12} xs={12} spacing={1} >
                <Typography style={{ color: "#d1c286" }}>
                    <b><ArrowRightIcon />NEW PRODUCT</b>
                </Typography>
                <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="center" spacing={2} xs={12} style={{ padding: 10 }}>
                    <Grid item  sm={2}>
                        <Typography>
                            <b>Name:</b>
                        </Typography>
                    </Grid>
                    <Grid item >
                        <TextField variant="outlined" label="Product name" defaultValue={productName} onChange={(e) => getProductName(e.target.value)} />
                    </Grid>
                </Grid>
                <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="center" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                    <Grid item sm={2}>
                        <Typography>
                            <b>Type:</b>
                        </Typography>
                    </Grid>
                    <Grid item >
                        <TextField variant="outlined" label="Product type" defaultValue={productType} onChange={(e) => getProductType(e.target.value)} />
                    </Grid>
                </Grid>
                <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="center" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                    <Grid item sm={2}>
                        <Typography >
                            <b>Description:</b>
                        </Typography>
                    </Grid>
                    <Grid item >
                        <TextField variant="outlined" label="Product description" defaultValue={description} onChange={(e) => getDescription(e.target.value)} />
                    </Grid>
                </Grid>
                <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="center" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                    <Grid item sm={2}>
                        <Typography>
                            <b>Price:</b>
                        </Typography>
                    </Grid>
                    <Grid item  >
                        <TextField variant="outlined" type="number" label="Product buy price" defaultValue={buyPrice} onChange={(e) => getBuyPrice(e.target.value)} />
                    </Grid>
                </Grid>
                <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="center" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                    <Grid item sm={2}>
                        <Typography>
                            <b>Promotion price:</b>
                        </Typography>
                    </Grid>
                    <Grid item >
                        <TextField variant="outlined" type="number" label="Product promotion price" defaultValue={promotionPrice} onChange={(e) => getPromotionPrice(e.target.value)} />
                    </Grid>
                </Grid>
                <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="center" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                    <Grid item sm={2}>
                        <Typography>
                            <b>Delivery fee:</b>
                        </Typography>
                    </Grid>
                    <Grid item >
                        <TextField variant="outlined" label="Delivery Fee" defaultValue={deliveryFee} onChange={(e) => getDeliveryFee(e.target.value)} />
                    </Grid>
                </Grid>
                <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="center" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                    <Grid item sm={2}>
                        <Typography>
                            <b>Deliver time:</b>
                        </Typography>
                    </Grid>
                    <Grid item >
                        <TextField sx={{ width: "300px" }} variant="outlined" label="Deliver Time" defaultValue={deliverTime} onChange={(e) => getDeliverTime(e.target.value)} />
                    </Grid>
                </Grid>
                <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="center" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                    <Grid item sm={2}>
                        <Typography>
                            <b>Warranty time:</b>
                        </Typography>
                    </Grid>
                    <Grid item >
                        <TextField variant="outlined" label="Product Warranty Time" defaultValue={warrantyTime} onChange={(e) => getWarrantyTime(e.target.value)} />
                    </Grid>
                </Grid>
                <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                            <Grid item sm={2}>
                                <Typography>
                                   <b> Product Amount:</b>
                                </Typography>
                            </Grid>
                                <Grid item>
                                    <TextField
                                        select
                                        label="Amount"
                                        value={amount}
                                        sx={{width:"200px"}}
                                        onChange={(e)=> {getAmount(e.target.value)}}
                                    >
                                        <MenuItem value={1}>
                                            <Typography>Available</Typography>
                                        </MenuItem>
                                        <MenuItem value={0}>
                                            <Typography>Out of Stock</Typography>
                                        </MenuItem>
                                    </TextField>
                                </Grid>
                                
                        </Grid>
                <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="center" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                    <Grid item sm={2}>
                        <Typography>
                            <b>Product description:</b>
                        </Typography>
                    </Grid>
                    <Grid item>
                        <TextareaAutosize
                            aria-label="minimum height"
                            minRows={3}
                            value={productDescription}
                            style={{ width: "500px", borderRadius: "10px", padding: 5 }}
                            onChange={(e) => getProductDescription(e.target.value)}
                        />
                    </Grid>
                </Grid>
                <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="center" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                    <Grid item sm={2}>
                        <Typography>
                            <b>Image product:</b>
                        </Typography>
                    </Grid>
                    <Grid item sm={10}>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10, marginTop: 10 }}>
                            <Grid container spacing={2} direction="row"
                                justifyContent="flex-start"
                                alignItems="center">

                                {imgList.map((url, index) => {
                                    return (
                                        <Grid item>
                                            <Grid item key={index} className="img-Box">
                                                    <label htmlFor={'id-checkbox'+ index}>
                                                        <img className="related-img"
                                                            src={url} alt="product" style={{ width: "200px", display: "block", marginLeft: "auto", marginRight: "auto", borderRadius: "40px", padding: 10 }}
                                                            onClick={(url) => getImgSelected(url.target.currentSrc)} />
                                                    </label>
                                                    <input id={'id-checkbox'+ index} name="image-product" type="radio" className="select-check"/>
                                                    <CheckCircleIcon className="icon-check"/>
                                                    
                                                <Grid item className="divColor" style={{ width: 35, height: 35 }} sm={12} xs={12}>
                                                    <Grid container className="icon-div" alignItems="center" justifyContent="center">
                                                        <DeleteOutlineIcon type="button" style={{ color: "white" }} onClick={() => {deleteImageHandler(url)}} />
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    )
                                })}
                                <Grid item>
                                    <Grid style={{ width: "180px", height: "180px", backgroundColor: "white", borderRadius: "30px", padding: 10 }}
                                        justifyContent="center"
                                        alignItems="center" container direction="row"
                                    >
                                        <Button style={{ color: "#d1c286" }} component="label">
                                            {imgUrl === null || imgUrl === undefined
                                                ?
                                                <>
                                                    <AddIcon style={{ fontSize: "50px", padding: 10 }} /><Typography>Add New Image </Typography>
                                                </>
                                                :
                                                <Grid justifyContent="flex-start"
                                                    alignItems="center" container
                                                    wrap="wrap">
                                                    <img src={imgUrl} /><Typography> {imgUrl.name.slice(0, 20) + '...'}</Typography>
                                                </Grid>
                                            }
                                            <input hidden multiple type="file" onChange={(e) => { getImgUrl(e.target.files[0]) }} accept="image/*" />
                                        </Button>
                                        <Grid item>
                                            <Button variant="contained" component="label" onClick={onImageUrlHandler}>
                                                Upload
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid container direction="row"
                    justifyContent="flex-end"
                    alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                    <Grid item>
                        <Button variant="contained" style={{backgroundColor:"#d1c286"}} onClick={onBtnSaveProduct}>
                            CREATE PRODUCT
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
        </>
    )
}
export default CreateProduct;