import { Breadcrumb, BreadcrumbItem } from "reactstrap"
import { Link, useParams } from "react-router-dom"
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCustomerById } from "../../../actions/account.action";
import { Grid, Typography, TableContainer, Table, TableRow, TableCell, TableBody, Paper, TableHead } from "@mui/material";
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
const DetailsOfCustomer = ()=>{
    const dispatch = useDispatch()
    const {userId} = useParams();
    const {customerDetail} = useSelector((reduxData)=>
        reduxData.accountReducer
    );

    useEffect(()=>{
        dispatch(getCustomerById(userId));
        console.log(customerDetail)
    }, [userId])

    //function tính giá trị tổng đơn hàng
    function total() {
        const valueOrder = []

        //lấy tổng giá trị order của khách hàng để phân loại loyalty
        if (Object.keys(customerDetail).length > 0) {
            customerDetail.Carts.map((arr) => {
                const i = arr.Orders.reduce((sumary, item) => sumary + item.Product.PromotionPrice * item.quantity, 0);
                valueOrder.push(i)
            })
        }
        return valueOrder.reduce((n, sum) => n + sum, 0)
    }

    //function lấy membership dựa trên tổng đơn hàng
    function loyalty(total){
        if(total>2000 && total< 5000){
            return "Gold"
        }
        if(total>500 && total<2000){
            return "Silver"
        }
        if(total>5000){
            return "Diamond"
        }
        if(total > 0 ){
            return "Member"
        }
        else{
            return "Not member"
        }
    }
    //console.log(loyalty(total()))
    return(
        <>
            {customerDetail.Address !== undefined? 
                <>
                <Breadcrumb>
                    <BreadcrumbItem >
                        <Link to="/Administrator/Customer" style={{ textDecoration: "none" }}>Customers</Link>
                    </BreadcrumbItem>
                    <BreadcrumbItem >
                        <Link >Detail customer</Link>
                    </BreadcrumbItem>
                </Breadcrumb>
                <Grid container direction="column"
                    justifyContent="flex-start"
                    alignItems="flex-start"
                    style={{backgroundColor: "whiteSmoke", padding: 20, borderRadius:"20px"}}
                    md={12} sm={12} lg={12} xs={12} spacing={1} >
                        <Typography style={{color:"#d1c286"}}>
                           <b><ArrowRightIcon/>CUSTOMER INFORMATION</b> 
                        </Typography>
                    <Grid container direction="row"
                        justifyContent="flex-start"
                        alignItems="flex-start" spacing={2} xs={12} style={{ padding: 10 }}>
                        <Grid item  >
                            <Typography>
                                Name:
                            </Typography>
                        </Grid>
                        <Grid item >
                            <Typography>
                                <b>{customerDetail.Name}</b>
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container direction="row"
                        justifyContent="flex-start"
                        alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                        <Grid item >
                            <Typography>
                                Email:
                            </Typography>
                        </Grid>
                        <Grid item >
                            <Typography>
                                <b>{customerDetail.Email}</b>
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container direction="row"
                        justifyContent="flex-start"
                        alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                        <Grid item >
                            <Typography>
                                Phone:
                            </Typography>
                        </Grid>
                        <Grid item >
                            <Typography>
                                <b>{customerDetail.Phone}</b>
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container direction="row"
                        justifyContent="flex-start"
                        alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                        <Grid item >
                            <Typography>
                                Gender:
                            </Typography>
                        </Grid>
                        <Grid item  >
                            <Typography>
                                <b>{customerDetail.Gender}</b>
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container direction="row"
                        justifyContent="flex-start"
                        alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                        <Grid item >
                            <Typography>
                                Birthday:
                            </Typography>
                        </Grid>
                        <Grid item >
                            <Typography>
                                <b>{customerDetail.Birthday}</b>
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container direction="row"
                        justifyContent="flex-start"
                        alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                        <Grid item >
                            <Typography>
                                Address:
                            </Typography>
                        </Grid>
                        <Grid item >
                            <Typography>
                                <b>{customerDetail.Address.Provide + ", " + customerDetail.Address.District + ", " + customerDetail.Address.Ward + ", " + customerDetail.Address.Address}</b>
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container direction="row"
                        justifyContent="flex-start"
                        alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                        <Grid item >
                            <Typography>
                                Total spent:
                            </Typography>
                        </Grid>
                        <Grid item >
                            <Typography>
                                <b>{total() === 0 ? 0: `$ ${total()}`}</b>
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container direction="row"
                        justifyContent="flex-start"
                        alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                        <Grid item >
                            <Typography>
                                Loyalty:
                            </Typography>
                        </Grid>
                        <Grid item >
                            <Typography>
                                <b>{loyalty(total())}</b>
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid container direction="column"
                    justifyContent="flex-start"
                    alignItems="flex-start"
                    style={{backgroundColor: "whiteSmoke", padding: 20, borderRadius:"20px", marginTop:"20px"}}
                    md={12} sm={12} lg={12} xs={12} spacing={1} >
                        <Grid item>
                            <Typography style={{color:"#d1c286"}}>
                            <b><ArrowRightIcon/>ORDERS</b> 
                            </Typography>
                        </Grid>
                        <Grid item>
                            <TableContainer component={Paper} className="form-admin-page">
                                <Table md={12} sm={12} lg={12} xs={12}>
                                    <TableHead>
                                        <TableRow style={{ backgroundColor: "#ecebc0" }}>
                                            <TableCell align="left"><b>Order code</b></TableCell>
                                            <TableCell align="left"><b>Order</b></TableCell>
                                            <TableCell align="left"><b>Order Created</b></TableCell>
                                            <TableCell align="left"><b>Status</b></TableCell>
                                            <TableCell align="left"><b>Total</b></TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {customerDetail.Carts.map((value, index) => {
                                                return(
                                                <TableRow
                                                
                                                key={index}
                                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                >
                                                    <TableCell component="th" scope="row">
                                                        {value._id.substr(value._id.length - 6)}
                                                    </TableCell>
                                                    
                                                    <TableCell align="left">{value.Orders.map((value, index)=>{
                                                        return(
                                                            <Grid container direction="column"
                                                                justifyContent="center"
                                                                alignItems="flex-start">
                                                                    <Grid item>
                                                                        <Grid container direction="row" justifyContent="flex-start"
                                                                                alignItems="center">
                                                                            <img src={value.Product.ImageUrl} style={{ width:"60px"}}/>
                                                                            <Typography>{value.Product.Name}</Typography>
                                                                        </Grid>
                                                                    </Grid>
                                                            </Grid>
                                                        )
                                                    })}</TableCell>
                                                    <TableCell align="left">{value.createdAt.slice(0,10)}</TableCell>
                                                    <TableCell align="left">{value.Status}</TableCell>
                                                    <TableCell align="left">$ {value.Orders.reduce((sumary, item) => sumary + item.Product.PromotionPrice*item.quantity, 0)}</TableCell>
                                                </TableRow>
                                                )
                                            })}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                    </Grid>
                </>
            :
                <>
                    <Breadcrumb>
                        <BreadcrumbItem >
                            <Link to="/Administrator/Customer" style={{ textDecoration: "none" }}>Customers</Link>
                        </BreadcrumbItem>
                        <BreadcrumbItem >
                            <Link >Detail customer</Link>
                        </BreadcrumbItem>
                    </Breadcrumb>
                    <Grid container direction="column"
                    justifyContent="flex-start"
                    alignItems="flex-start"
                    style={{backgroundColor: "whiteSmoke", padding: 20, borderRadius:"20px"}}
                    md={12} sm={12} lg={12} xs={12} spacing={1} >
                        <Typography style={{color:"#d1c286"}}>
                           <b><ArrowRightIcon/>CUSTOMER INFORMATION</b> 
                        </Typography>
                        <Grid container direction="row"
                        justifyContent="flex-start"
                        alignItems="center" spacing={2} xs={12} style={{ padding: 10 }}>
                            <Grid item  >
                                <Typography>
                                    There is no purchase history for this customer
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                </>}
        </>
    )
}
export default  DetailsOfCustomer 
    