import { Button, FormControl, Grid, InputLabel, MenuItem, Paper, Select, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Typography } from "@mui/material"
import { Link, useParams } from "react-router-dom"
import { Breadcrumb, BreadcrumbItem } from "reactstrap"
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { getCustomerByOrderId, getOrderResult } from "../../../actions/account.action";
import { getAddressDetail, getAddressUser, getDistrict, getProvide, getWard } from "../../../actions/product.action";
const fetchApi = async(url, body) =>{
    const response = await fetch(url,body);
    const data = await response.json();
    
    return data;
}
const OrderDetails = ()=>{
    const dispatch = useDispatch()
    const {orderId} = useParams();
    const {customerDetail, orderResult} = useSelector((reduxData)=>
        reduxData.accountReducer
    );
    const {ward, address, district, selectedDistrict, selectedProvide, selectedWard, selectedAddress} = useSelector((reduxData)=>
        reduxData.productReducer
    );
    const [editNewAddress, setEditNewAddress] = useState(false);
    const handleProvideChange = (e) =>{
        dispatch(getProvide(e.target.value));
    }
    const handleDistrictChange = (e) =>{
        dispatch(getDistrict(e.target.value));
    }
    const handleWardChange = (e)=>{
        dispatch(getWard(e.target.value))
    }
    const onAddressHandler = (e) =>{
        dispatch(getAddressDetail(e.target.value))
    }
    useEffect(()=>{
        dispatch(getCustomerByOrderId(orderId));
        dispatch(getOrderResult(orderId));
        
    }, [])
    const [total , setTotal]= useState(0)
    useEffect(()=>{
        if(orderResult.Orders){
            setTotal(orderResult.Orders.reduce((sumary, item)=> sumary+ item.Product.PromotionPrice*item.quantity, 0))
        }
    },[orderResult])
    
    useEffect(()=>{
        
        fetchApi("https://provinces.open-api.vn/api/?depth=3")
            .then((data)=>{
                dispatch(getAddressUser(data))
            })
            .catch((error)=>{
                console.log(error.message)
            });
    },[])
    return (
        <>
        {orderResult.Orders
         && 
         <>
            <Breadcrumb>
                <BreadcrumbItem >
                    <Link to="/Administrator/Orders" style={{ textDecoration: "none" }}>Orders</Link>
                </BreadcrumbItem>
                <BreadcrumbItem >
                    <Link>Detail order</Link>
                </BreadcrumbItem>
            </Breadcrumb>
            <Grid container
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="stretch" spacing={1}
                >
                <Grid item sm={6} >
                    <Grid container direction="row"
                        justifyContent="flex-start"
                        alignItems="flex-start" style={{ height:"100%",backgroundColor: "whiteSmoke", padding: 20, borderRadius: "20px"}}
                        >
                        <Typography style={{ color: "#d1c286" }}>
                            <b><ArrowRightIcon />CUSTOMER INFORMATION</b>
                        </Typography>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} xs={12} style={{ padding: 10 }} >
                            <Grid item  >
                                <Typography>
                                    Name:
                                </Typography>
                            </Grid>
                            <Grid item >
                                <Typography>
                                    <b>{customerDetail.Name}</b>
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                            <Grid item >
                                <Typography>
                                    Email:
                                </Typography>
                            </Grid>
                            <Grid item >
                                <Typography>
                                    <b>{customerDetail.Email}</b>
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                            <Grid item >
                                <Typography>
                                    Phone:
                                </Typography>
                            </Grid>
                            <Grid item >
                                <Typography>
                                    <b>{customerDetail.Phone}</b>
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                            <Grid item >
                                <Typography>
                                    Gender:
                                </Typography>
                            </Grid>
                            <Grid item  >
                                <Typography>
                                    <b>{customerDetail.Gender}</b>
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                            <Grid item >
                                <Typography>
                                    Birthday:
                                </Typography>
                            </Grid>
                            <Grid item >
                                <Typography>
                                    <b>{customerDetail.Birthday}</b>
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                            <Grid item >
                                <Typography>
                                    Address:
                                </Typography>
                            </Grid>
                            <Grid item >
                                <Typography>
                                    <b>{customerDetail.Address.Provide + ", " + customerDetail.Address.District + ", " + customerDetail.Address.Ward + ", " + customerDetail.Address.Address}</b>
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="center" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                            <Grid item >
                                <Typography>
                                    New Address:
                                </Typography>
                            </Grid>
                            {editNewAddress === true ?
                                <Grid container spacing={2} direction="row" alignItems="flex-start">
                                    <Grid item md={12} sm={12} lg={12} xs={12}>
                                        <Grid container direction="column" alignItems="center" spacing={1} sm={12}>
                                            <Grid item sm={6}>
                                                <Grid item sm={3} mb={1}>
                                                    Province:
                                                </Grid>
                                                <Grid item sm={9}>
                                                    <FormControl fullWidth>
                                                        <InputLabel>Choose province</InputLabel>
                                                        <Select
                                                            label="Choose province"
                                                            value={selectedProvide}
                                                            onChange={handleProvideChange}
                                                            sx={{ width: 250 }}
                                                        >
                                                            {address.map((value, index) => {
                                                                return (
                                                                    <MenuItem value={value.name} key={index}>{value.name}</MenuItem>
                                                                )
                                                            })}
                                                        </Select>
                                                    </FormControl>
                                                </Grid>
                                            </Grid>
                                            <Grid item sm={6}>
                                                <Grid item sm={3} mb={1}>
                                                    District:
                                                </Grid>
                                                <Grid item sm={9}>
                                                    <FormControl fullWidth>
                                                        <InputLabel>Choose district</InputLabel>
                                                        <Select
                                                            value={selectedDistrict}
                                                            label="Choose province"
                                                            onChange={handleDistrictChange}
                                                            sx={{ width: 250 }}
                                                        >
                                                            {district.map((value, index) => {
                                                                return (
                                                                    <MenuItem value={value.name} key={index}>{value.name}</MenuItem>
                                                                )
                                                            })}
                                                        </Select>
                                                    </FormControl>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item md={12} sm={12} lg={12} xs={12}>
                                        <Grid container direction="column" alignItems="center" spacing={1} sm={12}>
                                            <Grid item sm={6}>
                                                <Grid item sm={3} mb={1}>
                                                    Ward:
                                                </Grid>
                                                <Grid item sm={9}>
                                                    <FormControl >
                                                        <InputLabel>Choose ward</InputLabel>
                                                        <Select
                                                            value={selectedWard}
                                                            label="Choose ward"
                                                            onChange={handleWardChange}
                                                            sx={{ width: 250 }}
                                                        >
                                                            {ward.map((value, index) => {
                                                                return (
                                                                    <MenuItem value={value.name} key={index}>{value.name}</MenuItem>
                                                                )
                                                            })}
                                                        </Select>
                                                    </FormControl>
                                                </Grid>
                                            </Grid>
                                            <Grid item sm={6}>
                                                <Grid item sm={3} mb={1}>
                                                    Address:
                                                </Grid>
                                                <Grid item sm={9}>
                                                    <FormControl >
                                                        <TextField value={selectedAddress} sx={{ width: 250 }} label="Address" variant="outlined" onChange={onAddressHandler} />
                                                    </FormControl>
                                                </Grid>
                                            </Grid>
                                            <Grid container justifyContent="flex-end">
                                                <Button onClick={()=>{setEditNewAddress(false)}}>SAVE</Button>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                :
                                <Grid item >
                                    <Button onClick={()=>setEditNewAddress(true)}>
                                        ADD NEW ADDRESS
                                    </Button>
                                </Grid>
                            }
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item sm={6}>
                    <Grid container direction="row"
                        justifyContent="flex-start"
                        alignItems="flex-start"
                        style={{height:"100%", backgroundColor: "whiteSmoke", padding: 20, borderRadius: "20px" }}
                    >
                        <Typography style={{ color: "#d1c286" }}>
                            <b><ArrowRightIcon />ORDER</b>
                        </Typography>
                            <TableContainer component={Paper} style={{width:"100%"}}>
                                <Table>
                                    <TableHead style={{ backgroundColor: "whiteSmoke"}}>
                                        <TableRow>
                                            <TableCell align="left"><b>Products</b></TableCell>
                                            <TableCell align="left" ><b>Price</b></TableCell>
                                            <TableCell align="left" ><b>Quantity</b></TableCell>
                                            <TableCell align="left" ><b>Total</b></TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {orderResult.Orders.map((value, index) => (
                                            <TableRow
                                                key={index}
                                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                            >
                                                <TableCell component="th" scope="row" align="left" style={{ width: "80%" }}><img src={value.Product.ImageUrl} style={{ height: "auto", width: "100px" }} /> &nbsp;&nbsp;{value.Product.Name}</TableCell>
                                                <TableCell align="left" style={{ width: "100%", paddingRight: 0 }}>$ {value.Product.PromotionPrice}</TableCell>
                                                <TableCell align="left" style={{ width: "100%" }}>{value.quantity}</TableCell>
                                                <TableCell align="left" style={{ width: "100%", paddingRight: 0 }}> $ {value.Product.PromotionPrice * value.quantity}</TableCell>
                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                            <Grid container sx={{ width: "100%" }}
                                direction="row"
                                marginTop={3}>
                                <Grid item marginRight={2}>
                                    <b>Total:</b>
                                </Grid>
                                <Grid item>
                                    <b style={{ color: "#d1c286" }}>$ {total}</b>
                                </Grid>
                            </Grid>
                        </Grid>
                </Grid>
                <Grid item sm={6}>
                <Grid container direction="column"
                        justifyContent="center"
                        alignItems="flex-start"
                        style={{ backgroundColor: "whiteSmoke", padding: 20, borderRadius: "20px" }}
                    >
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} xs={12} style={{ padding: 10 }}>
                            <Grid item  >
                                <Typography>
                                    Status:
                                </Typography>
                            </Grid>
                            <Grid item >
                                <Typography>
                                    <b>{orderResult.Status}</b>
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} xs={12} style={{ padding: 10 }}>
                            <Grid item  >
                                <Typography>
                                    Ordered Date:
                                </Typography>
                            </Grid>
                            <Grid item >
                                <Typography>
                                    <b>{orderResult.createdAt.slice(0,10)}</b>
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} xs={12} style={{ padding: 10 }}>
                            <Grid item  >
                                <Typography>
                                    Ordered Time:
                                </Typography>
                            </Grid>
                            <Grid item >
                                <Typography>
                                    <b>{orderResult.createdAt.slice(11,19)}</b>
                                </Typography>
                            </Grid>
                        </Grid>
                </Grid>
                </Grid>
            </Grid>
            </>
            }
        </>
    )
}
export default OrderDetails