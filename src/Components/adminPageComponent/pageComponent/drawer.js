import * as React from 'react';
import { createTheme, styled, ThemeProvider, useTheme } from '@mui/material/styles';
import {Box, Drawer, CssBaseline, Toolbar, List, Typography, Divider, ListItem, ListItemIcon, ListItemText, ListItemButton, Grid, Popover } from '@mui/material';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import NotificationsIcon from '@mui/icons-material/Notifications';
import avarta from "../../../assets/about/person_3.jpg";
import StorefrontIcon from '@mui/icons-material/Storefront';
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import InventoryIcon from '@mui/icons-material/Inventory';
import { useDispatch, useSelector } from 'react-redux';
import { getNumberProductNoti, logoutAccount } from '../../../actions/account.action';
import { NavLink } from 'react-router-dom';
const fetchApi = async(url, body) =>{
  const response = await fetch(url,body);
  const data = await response.json();
  
  return data;
}
//width của drawer mui
const drawerWidth = 180;

const themePage = createTheme({
  typography: {
    allVariants: {
      fontFamily: 'Comfortaa',
      textTransform: 'none',
    },
  },
});
const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
      flexGrow: 1,
      padding: theme.spacing(3),
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      marginLeft: `-${drawerWidth}px`,
      ...(open && {
        transition: theme.transitions.create('margin', {
          easing: theme.transitions.easing.easeOut,
          duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
      }),
    }),
  );

  const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
  })(({ theme, open }) => ({
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: `${drawerWidth}px`,
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    }),
  }));
  
  const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'space-between'
  }));
  
  export default function PersistentDrawerLeft({children}) {
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);
    const {numberProduct, toast, account} = useSelector((reduxData)=>
        reduxData.accountReducer
    )
    const dispatch = useDispatch();
    const handleDrawerOpen = () => {
      setOpen(true);
    };
  
    const handleDrawerClose = () => {
      setOpen(false);
    };
   
    const refToken = localStorage.getItem("refToken")
    const [anchorEl, setAnchorEl] = React.useState(null);
    React.useEffect(()=>{
      var requestOptions = {
        method: 'GET',
        headers:{"Authorization": `Bearer ${account.accessToken}`},
        redirect: 'follow'
      };
      
      fetchApi(process.env.REACT_APP_API_SHOP_24H + "/api" + "/orders", requestOptions)
        .then((data)=>{
            const numberNoftication = data.data.filter(value=> value.Status ==="Ordered")
            dispatch(getNumberProductNoti(numberNoftication.length))
        })
        .catch((error) => {
            console.log( error.message)
        });
    },[refToken, toast])
      const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
      };

      const handleClose = () => {
        setAnchorEl(null);
      };

      const openPopover = Boolean(anchorEl);
      const id = openPopover ? 'simple-popover' : undefined;
      const onLogoutHandler =() =>{
        dispatch(logoutAccount(refToken));
      }
    return (
      <>
        <ThemeProvider theme={themePage}>
           
          <Box sx={{ display: 'flex' }} md={12} sm={12} lg={12} xs={12}>
            <CssBaseline />
            <AppBar position="fixed" open={open} sx={{ backgroundColor:"#d1c286"}} >
              <Toolbar>
                <IconButton
                  color="inherit"
                  aria-label="open drawer"
                  onClick={handleDrawerOpen}
                  edge="start"
                  sx={{ mr: 2, ...(open && { display: 'none'}) }}
                >
                  <MenuIcon />
                </IconButton>
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                    >
                        <Grid item md={9} sm={9} lg={9} xs={9}>
                            <Typography noWrap component="div">
                                <h3 style={{margin: 0}}>Administrator Cosy Home</h3>
                            </Typography>
                        </Grid>
                        
                        <Grid item  md={3} sm={3} lg={3} xs={3}>
                            
                            <Grid container 
                                direction="row"
                                alignItems="center"
                                justifyContent="flex-end">
                                <Typography  component="div">
                                  <NotificationsIcon/>
                                  {numberProduct !== 0?<span className='badge badge-warning' id='lblCartCount'> {numberProduct} </span>:null}
                                </Typography>
                            </Grid>
                        </Grid>
                </Grid>
              </Toolbar>
            </AppBar>
            <Drawer
              sx={{
                width: drawerWidth,
                flexShrink: 0,
                '& .MuiDrawer-paper': {
                  width: drawerWidth,
                  boxSizing: 'border-box',
                  backgroundColor: "whiteSmoke"

                },
              }}
              variant="persistent"
              anchor="left"
              open={open}
            >
              <DrawerHeader>
              <Typography type="button" component="div" md={2} sm={2} lg={2} xs={2}
              aria-describedby={id}
              onClick={handleClick}>
                  <img src= {avarta} style={{width:"50px", borderRadius:"50%"}}/>
              </Typography>
              <Popover id={id} open={openPopover}
                  anchorEl={anchorEl}
                  onClose={handleClose}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'left',
                }}
              >
                <Typography type="button" onClick={onLogoutHandler} style={{color:"#d1c286", padding:5}}>Log out</Typography>
              </Popover>
                <IconButton onClick={handleDrawerClose}>
                  {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                </IconButton>
              </DrawerHeader>
              <Divider />
              <List>
                  <ListItem disablePadding>
                    <NavLink to="/Administrator/Orders" style={{textDecoration:"none"}}>
                      <ListItemButton >
                          <ListItemIcon>
                          <StorefrontIcon/> &nbsp;<b>Orders</b>
                          </ListItemIcon>
                          <ListItemText />
                      </ListItemButton>
                    </NavLink>
                  </ListItem>
                  <ListItem disablePadding>
                  <NavLink to="/Administrator/Customer" style={{textDecoration:"none"}}>
                      <ListItemButton  >
                          <ListItemIcon>
                            <PeopleAltIcon/>&nbsp;<b>Customers</b>
                          </ListItemIcon>
                          <ListItemText />
                      </ListItemButton>
                    </NavLink>
                  </ListItem>
                <ListItem disablePadding>
                  <NavLink to="/Administrator/Staff" style={{ textDecoration: "none" }}>
                    <ListItemButton >
                      <ListItemIcon>
                        <InventoryIcon />&nbsp;<b>Products</b>
                      </ListItemIcon>
                      <ListItemText />
                    </ListItemButton>
                  </NavLink>
                </ListItem>
              </List>
              <Divider />
            </Drawer>
            <Main open={open}>
              <DrawerHeader />
              {children}
            </Main>
          </Box>
        </ThemeProvider>
      </>
    );
  }