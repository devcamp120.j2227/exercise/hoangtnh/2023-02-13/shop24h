import { ADD_PRODUCT_TO_CART, BTN_CHECKOUT, CHECK_CART_STATUS, CHECK_ORDER_STATUS, CONTINUE_SHOPPING_CLICK,
    CREATE_ORDER_OF_CUSTOMER, DELETE_PRODUCT_IN_CART, EDIT_MINUS_QUANTITY_PRODUCT, EDIT_PLUS_QUANTITY_PRODUCT,
    FETCH_API, GET_ADDRESS, GET_ADDRESS_DETAIL, GET_CONDITION_API, GET_DISTRICT, GET_PROFILE, GET_PROVIDE,
    GET_WARD, LOGIN_USER, OPEN_TOAST, PAYMENT_METHOD, PRODUCT_CLICK_HANDLER, QUANTITY_PRODUCT_HANDLER,
    SET_BIRTHDAY, SET_GENDER, SET_NAME, SET_PAGE, SET_PHONE, GET_LENGTH, CHANGE_STATUS_FILTER, HANDLE_CLOSE_FILTER_PRODUCT } from "../constants/product.const";

export default function getCondition (data) {
    return{
        type: GET_CONDITION_API,
        payload: data
    }
};
export const getProductInfor = (data) =>{
    return {    
        type: PRODUCT_CLICK_HANDLER,
        payload: data
    }
}
export const quantityButtonHandler = (data) =>{
    return{
        type: QUANTITY_PRODUCT_HANDLER,
        payload: data
    }
}
export const addProductToCartHandler = (data) =>{
    return{
        type: ADD_PRODUCT_TO_CART,
        payload: data
    }
}
export const loginUserInformation = (data) =>{
    return{
        type: LOGIN_USER,
        payload: data
    }
}
export const changeCartStatus = () =>{
    return {
        type: CHECK_CART_STATUS
    }
}
export const onBtnPlusHandler = (id) =>{
    return {
        type: EDIT_PLUS_QUANTITY_PRODUCT,
        payload: id
    }
}
export const onBtnMinusHandler = (id) =>{
    return{
        type: EDIT_MINUS_QUANTITY_PRODUCT,
        payload: id
    }
}
export const onBtnRemoveProduct = (id) =>{
    return{
        type: DELETE_PRODUCT_IN_CART,
        payload: id
    }
}
export const getProfileUser = (data) =>{
    return{
        type: GET_PROFILE,
        payload: data
    }
}
export const fetchApiProfileUser = (userInfor) =>{
    return async(dispatch)=>{
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify({
                UserId: userInfor.userProfile,
                Name: userInfor.name,
                Email: userInfor.email,
                Birthday: userInfor.birthday,
                Phone: userInfor.phone,
                Gender: userInfor.gender,
                Address: "",
                }),
            redirect: 'follow'
            };
            try {
                const responsePost =  await fetch(process.env.REACT_APP_API_SHOP_24H + "/api" + "/customers", requestOptions);
                const dataPost = await responsePost.json();
                if(dataPost.status === 'Internal server error'){
                    var body = {
                        method: 'PUT',
                        headers: {"Content-Type": "application/json"},
                        body: JSON.stringify({
                            Name: userInfor.name,
                            Email: userInfor.email,
                            Birthday: userInfor.birthday,
                            Phone: userInfor.phone,
                            Gender: userInfor.gender,
                            Address: userInfor.address
                        }),
                        redirect: 'follow'
                      };
                    try {
                        const responsePut = await fetch(process.env.REACT_APP_API_SHOP_24H + "/api" + "/customers?userId="+`${userInfor.userProfile}`, body);
                        const dataPut = await responsePut.json();
                         dispatch({
                            type: FETCH_API,
                            payload: dataPut
                        })
                    } catch (error) {
                         dispatch({
                            error: error
                        })
                    }
                }
                 dispatch({
                    type: FETCH_API,
                    payload: dataPost,
                })
            } catch (error) {
                 dispatch({
                    error: error
                })
            }
    }
}
export const getAddressUser = (data) =>{
    return{
        type: GET_ADDRESS,
        payload: data
    }
}
export const getProvide= (data) =>{
    return{
        type: GET_PROVIDE,
        payload: data
    }
}
export const getDistrict= (data) =>{
    return{
        type: GET_DISTRICT,
        payload: data
    }
}
export const getWard= (data) =>{
    return{
        type: GET_WARD,
        payload: data
    }
}
export const getAddressDetail = (data) =>{
    return{
        type: GET_ADDRESS_DETAIL,
        payload: data
    }
}
export const setPage = (data) =>{
    return{
        type: SET_PAGE,
        payload: data
    }
}
export const setName = (data) =>{
    return{
        type: SET_NAME,
        payload: data
    }
}
export const setBirthday = (data) =>{
    return{
        type: SET_BIRTHDAY,
        payload: data
    }
}
export const setGender = (data) =>{
    return{
        type: SET_GENDER,
        payload: data
    }
}
export const setPhone = (data) =>{
    return{
        type: SET_PHONE,
        payload: data
    }
}
export const btnCheckOutHandler = (data) =>{
    return{
        type: BTN_CHECKOUT,
        payload: data
    }
}
export const selectPaymentMethod = (data)=>{
    return {
        type: PAYMENT_METHOD,
        payload: data
    }
}
export const createOrder = (userId, cartBag) =>{
    return async(dispatch)=>{
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify({
                "Orders": cartBag
                }),
            redirect: 'follow'
            };
            try {
                const responsePost =  await fetch(process.env.REACT_APP_API_SHOP_24H + "/api" + "/orders/post?userId="+`${userId}`, requestOptions);
                const data = await responsePost.json();
                localStorage.removeItem("item");
                 dispatch({
                    type: CREATE_ORDER_OF_CUSTOMER,
                    payload: data.data,
                })
            } catch (error) {
                 dispatch({
                    error: error
                })
            }
    }
}
export const continueShopping = (data) =>{
    return{
        type: CONTINUE_SHOPPING_CLICK,
        payload: data
    }
}
export const checkOrderStatus = (data) =>{
       return{
        type: CHECK_ORDER_STATUS,
        payload: data
       }
}
export const openToastAddCart = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: OPEN_TOAST,
            data: data
        })
    }
}
export const getLength = (length)=>{
    return async(dispatch) =>{
        if(!length){
            try{
                var requestOptions = {
                    method: 'GET',
                    headers: {"Content-Type": "application/json"},
                    redirect: 'follow'
                }
                const response = await fetch (process.env.REACT_APP_API_SHOP_24H + "/api" + "/skip-limit-products", requestOptions);
                const data = await response.json();
                await dispatch({
                    type: GET_LENGTH,
                    payload: data.data.length
                })
            }catch  (error){
                console.log(error)
            }
        }
        else{
            await dispatch({
                type: GET_LENGTH,
                payload: length
            }) 
        }
    }
}
export const changeStatusFiter = ()=>{
    return {
        type: CHANGE_STATUS_FILTER,
        payload: false
    }
}
export const handleCloseFilter =(boolean)=>{
    return {
        type: HANDLE_CLOSE_FILTER_PRODUCT,
        payload: boolean
    }
}
