import { onBtnRemoveProduct } from "../../../actions/account.action";
import { Grid, TableContainer, Table, TableRow, TableCell, TableBody, Paper, TableHead } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import ClearIcon from '@mui/icons-material/Clear';
const CartBagTable = ()=>{
    const dispatch = useDispatch();
    const {cartBag} = useSelector((reduxData) =>
        reduxData.accountReducer
    );
    //hàm xử lý nút xóa sản phẩm
    const onBtnDelProduct =(e) =>{

        dispatch(onBtnRemoveProduct(e.Product._id));
    }
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    const total = numberWithCommas(cartBag.reduce((sumary, item)=> sumary+ item.Product.PromotionPrice*item.quantity, 0));
    return (
        <>
            <TableContainer component={Paper}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell align="left"><b>Products</b></TableCell>
                            <TableCell align="left" ><b>Price</b></TableCell>
                            <TableCell align="left" ><b>Quantity</b></TableCell>
                            <TableCell align="left" ><b>Total</b></TableCell>
                            <TableCell align="left" ></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {cartBag.map((value, index) => (
                        <TableRow
                        key={index}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                        <TableCell component="th" scope="row" align="left"style={{width:"80%"}}><img src={value.Product.ImageUrl} style={{height:"auto", width:"100px"}}/> &nbsp;&nbsp;{value.Product.Name}</TableCell>
                        <TableCell align="left"style={{width:"100%",paddingRight:0  }}>$ {value.Product.PromotionPrice}</TableCell>
                        <TableCell align="left" style={{width:"100%"}}>{value.quantity}</TableCell>
                        <TableCell align="left" style={{width:"100%",paddingRight:0}}> $ {value.Product.PromotionPrice * value.quantity}</TableCell>
                        <TableCell align="left"><ClearIcon type="button" onClick= {()=> onBtnDelProduct(value)}/></TableCell>
                        </TableRow>
                    ))} 
                    </TableBody>
                </Table>
            </TableContainer>
            <Grid container sx={{ width: "100%" }}
                direction="row"
                marginBottom={5}
                marginTop={3}>
                <Grid item sm={1}>
                    <b>Total:</b>
                </Grid>
                <Grid item>
                    <b style={{ color: "#d1c286" }}>$ {total}</b>
                </Grid>
            </Grid>
        </>
    )
}

export default CartBagTable;