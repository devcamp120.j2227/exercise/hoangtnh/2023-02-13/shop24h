import { Grid, ThemeProvider,  FormControlLabel, RadioGroup, Radio, Button, Box, Typography, Modal, Input} from "@mui/material";
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { addProductToCartHandler, continueShopping, createOrder, fetchApiProfileUser, selectPaymentMethod,setPhone } from "../../actions/product.action";
import { theme } from "./userProfile";
import EditIcon from '@mui/icons-material/Edit';
import { useNavigate } from "react-router-dom";
import avatarDefault from "../../assets/default avatar/png-transparent-default-avatar-thumbnail.png"

export const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };
const Payment = () =>{
    const {selectedDistrict, selectedProvide, selectedWard, selectedAddress, cartBag, payment, user, newOrder, phoneLoad, loadProfile} = useSelector((reduxData)=>
            reduxData.productReducer
    )
    const {accountUser} = useSelector ((reduxData)=>
        reduxData.accountReducer  
    )
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    const total = numberWithCommas(cartBag.reduce((sumary, item)=> sumary+ item.Product.PromotionPrice*item.quantity, 0));
    const dispatch =  useDispatch();
    const navigate = useNavigate()
    const [open, setOpen] = useState(false);
    const [phoneNumber , getPhoneNumber] = useState(phoneLoad);

    const [uid, setUid] = useState("");
    const [email, setEmail] = useState("");
    const [photoURL, setPhotoUrl] = useState("");
    const [displayName, setDisplayName] = useState("");
    const [birthday, setBirthday] = useState("");
    const [gender, setGender] = useState("")
    useEffect(()=>{
        if(accountUser.data !== undefined){
            setUid(accountUser.data.exitUser._id)
            setEmail(accountUser.data.exitUser.Email);
            setPhotoUrl(avatarDefault)
            setDisplayName(accountUser.data.exitUser.Username);
            setGender(accountUser.data.exitUser.Gender);
            setBirthday(accountUser.data.exitUser.Birthday)
        }
        if(user){
            setUid(user.uid)
            setEmail(user.email)
            setPhotoUrl(user.photoURL)
            setDisplayName(user.displayName)
            setGender(loadProfile.Gender);
            setBirthday(loadProfile.Birthday)
        }
    }, [accountUser, user])
    const onCashSelected =(e) =>{
        dispatch(selectPaymentMethod(e.target.value))
    }
    const onCardSelected =(e) =>{
        dispatch(selectPaymentMethod(e.target.value))
    }
    const onBtnConfirm = () =>{
        if(payment===""){
            return setOpen(true)
        }
        if(phoneNumber ===""){
            return setOpen(true)
        }
        else{
            var userInfor = {
                userProfile: uid,
                phone: phoneNumber,
                name: displayName,
                email: email,
                birthday: birthday,
                gender: gender,
            }
            cartBag[0].payment = payment;
            dispatch(fetchApiProfileUser(userInfor))
            dispatch(createOrder(uid, cartBag));
            dispatch(addProductToCartHandler([]));
        }
    }
    const onBtnContinueShopping = () =>{
        navigate("/Products");
        dispatch(continueShopping(null))
    }
    const handleClose = () => setOpen(false);
    useEffect(()=>{}, [user])
    return(
        <>
        <Grid container direction="column" className="user-payment-desktop"
            justifyContent="flex-start" alignItems="flex-start"
            md={9} sm={9} lg={9} xs={9} style={{ backgroundColor: "whiteSmoke", padding: 10, marginBottom: 20 }}>
            <Grid item>
                <h3><b>My Order</b></h3>
            </Grid>
            <Grid item style={{ fontSize: 12, fontStyle: "italic" }}>
                Check your order and payment method
            </Grid>
            <br />
            <Grid container direction="column" md={12} sm={12} lg={12} xs={12}>
                {newOrder !== null ? <>
                    <Grid item mt={2}>
                        Thanks for choosing <b style={{ color: "#d1c286" }}>Cosy Home </b>. This is your order code: <b>{newOrder._id.substr(newOrder._id.length - 6)}</b>
                    </Grid>
                    <Grid item>
                        <Grid container justifyContent="flex-end">
                            <ThemeProvider theme={theme}>
                                <Button variant="contained" style={{ backgroundColor: "#d1c286" }} onClick={onBtnContinueShopping}>Continue Shopping</Button>
                            </ThemeProvider>
                        </Grid>
                    </Grid>
                </>
                    : <>
                        <Grid item>
                            • Bag:
                        </Grid>
                        <Grid item>
                            <Grid container direction="row" alignItems="center">
                                {cartBag.map((value, index) => {
                                    return (
                                        <>
                                            <Grid item sm={8} alignItems="center">
                                                <img src={value.Product.ImageUrl} style={{ width: "35px", borderRadius: "25%", marginTop: 2 }} />                            &nbsp;
                                                &nbsp;
                                                <b>{value.Product.Name}</b> x <b>{value.quantity}</b> 
                                            </Grid>
                                            <Grid item sm={4} alignItems="center">
                                                <b>${value.quantity * value.Product.PromotionPrice}</b>
                                            </Grid>
                                        </>
                                    )
                                })}
                                <Grid item mt={2}>
                                    • Total: <b>${total}</b>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item mt={2}>
                            • Payment:
                        </Grid>
                        <Grid item>
                            {payment !== "" ?
                                <Grid container direction="row">
                                    <b>{payment}</b>
                                    &nbsp;
                                    <EditIcon type="button" style={{ fontSize: "medium", color: "blue", marginTop: "-5px" }} onClick={() => dispatch(selectPaymentMethod(""))} />
                                    <p style={{ fontSize: "10px", color: "blue", marginBottom: 0, marginTop: "-5px" }}>edit</p>
                                </Grid>
                                :
                                <ThemeProvider theme={theme}>
                                    <RadioGroup row>
                                        <FormControlLabel value="By Cash" control={<Radio />} label="By cash" onChange={(e) => onCashSelected(e)} />
                                        <FormControlLabel value="By Cash" control={<Radio />} label="By card" onChange={(e) => onCardSelected(e)} />
                                    </RadioGroup>
                                </ThemeProvider>
                            }
                        </Grid>
                        <Grid item style={{ alignItems: "center", display: "flex", direction: "row" }} mt={1}>
                            • Phone number: &nbsp;
                            {phoneLoad === "" || phoneLoad === undefined ?
                                <Input type="number" value={phoneNumber} onChange={(value) => getPhoneNumber(value.target.value)} sx={{ width: "50%" }} />
                                : <>
                                    <b>{phoneLoad}</b>
                                    &nbsp;
                                    <EditIcon style={{ fontSize: "medium", color: "blue", marginTop: "-15" }} type="button" onClick={() => dispatch(setPhone(""))} />
                                    <p style={{ fontSize: "10px", color: "blue", marginBottom: 0, marginTop: "-15px" }}>edit</p>
                                </>}
                        </Grid>
                        <Grid item mt={1}>
                            • Delivery address: <b>{selectedProvide}, {selectedDistrict}, {selectedWard}, {selectedAddress}.</b>
                        </Grid>
                        <Grid item mt={1}>
                            <Grid container justifyContent="flex-end">
                                <ThemeProvider theme={theme}>
                                    <Button variant="contained" style={{ backgroundColor: "#d1c286" }} onClick={onBtnConfirm}>Confirm & Create Order</Button>
                                </ThemeProvider>
                            </Grid>
                        </Grid>
                    </>
                }
            </Grid>

            <ThemeProvider theme={theme}>
                <Modal
                    open={open}
                    onClose={handleClose}
                >
                    <Box sx={style}>
                        {
                            payment === ""
                                ?
                                <Typography id="modal-modal-title" variant="h6" component="h2">
                                    <b>Kindly choose Payment method</b>
                                </Typography>
                                :
                                phoneLoad === ""
                                    ?
                                    <Typography id="modal-modal-title" variant="h6" component="h2">
                                        <b>Kindly Give Phone Number</b>
                                    </Typography>
                                    :
                                    null
                        }

                    </Box>
                </Modal>
            </ThemeProvider>
        </Grid>

        <Grid container direction="row" className="user-payment-mobile"
            justifyContent="flex-start" alignItems="flex-start"
            xs={12} style={{ backgroundColor: "whiteSmoke", padding: 10, marginBottom: 20 }}>
            <Grid item>
                <h3><b>My Order</b></h3>
            </Grid>
            <Grid item style={{ fontSize: 12, fontStyle: "italic" }}>
                Check your order and payment method
            </Grid>
            <br />
            <Grid container direction="column" md={12} sm={12} lg={12} xs={12}>
                {newOrder !== null ? <>
                    <Grid item mt={2}>
                        Thanks for choosing <b style={{ color: "#d1c286" }}>Cosy Home </b>. This is your order code: <b>{newOrder._id.substr(newOrder._id.length - 6)}</b>
                    </Grid>
                    <Grid item>
                        <Grid container justifyContent="flex-end">
                            <ThemeProvider theme={theme}>
                                <Button variant="contained" style={{ backgroundColor: "#d1c286" }} onClick={onBtnContinueShopping}>Continue Shopping</Button>
                            </ThemeProvider>
                        </Grid>
                    </Grid>
                </>
                    : <>
                        <Grid item>
                            • Bag:
                        </Grid>
                        <Grid item>
                            <Grid container direction="row" alignItems="center" justifyContent="space-between">
                                {cartBag.map((value, index) => {
                                    return (
                                        <>
                                            <Grid item style={{fontSize:"smaller"}}>
                                                <img src={value.Product.ImageUrl} style={{ width: "35px", borderRadius: "25%", marginTop: 2 }} />                            &nbsp;
                                                &nbsp;
                                                <b >{value.Product.Name}</b> x <b>{value.quantity}</b> 
                                            </Grid>
                                            <Grid item style={{fontSize:"smaller"}}>
                                                <b>$ {value.quantity * value.Product.PromotionPrice}</b>
                                            </Grid>
                                        </>
                                    )
                                })}
                                <Grid item mt={2}>
                                    • Total: <b>${total}</b>
                                </Grid>
                            </Grid>
                        </Grid>
                            <Grid item mt={2}>
                                • Payment:
                            </Grid>
                            <Grid item ml={5}>
                                {payment !== "" ?
                                    <Grid container direction="row">
                                        <b>{payment}</b>
                                        &nbsp;
                                        <EditIcon type="button" style={{ fontSize: "medium", color: "blue", marginTop: "-5px" }} onClick={() => dispatch(selectPaymentMethod(""))} />
                                        <p style={{ fontSize: "10px", color: "blue", marginBottom: 0, marginTop: "-5px" }}>edit</p>
                                    </Grid>
                                    :
                                    <ThemeProvider theme={theme}>
                                        <RadioGroup row>
                                            <FormControlLabel value="By Cash" control={<Radio />} label="By cash" onChange={(e) => onCashSelected(e)} />
                                            <FormControlLabel value="By Cash" control={<Radio />} label="By card" onChange={(e) => onCardSelected(e)} />
                                        </RadioGroup>
                                    </ThemeProvider>
                                }
                            </Grid>
                        <Grid item style={{ alignItems: "center", display: "flex", direction: "row" }} mt={2}>
                            • Phone number: &nbsp;
                            {phoneLoad === "" || phoneLoad === undefined ?
                                <Input type="number" value={phoneNumber} onChange={(value) => getPhoneNumber(value.target.value)} sx={{ width: "50%" }} />
                                : <>
                                    <b>{phoneLoad}</b>
                                    &nbsp;
                                    <EditIcon style={{ fontSize: "medium", color: "blue", marginTop: "-15" }} type="button" onClick={() => dispatch(setPhone(""))} />
                                    <p style={{ fontSize: "10px", color: "blue", marginBottom: 0, marginTop: "-15px" }}>edit</p>
                                </>}
                        </Grid>
                        <Grid item mt={2}>
                            • Delivery address: <b>{selectedProvide}, {selectedDistrict}, {selectedWard}, {selectedAddress}.</b>
                        </Grid>
                        <Grid item mt={2}>
                            <Grid container justifyContent="flex-end">
                                <ThemeProvider theme={theme}>
                                    <Button variant="contained" style={{ backgroundColor: "#d1c286" }} onClick={onBtnConfirm}>Confirm & Create Order</Button>
                                </ThemeProvider>
                            </Grid>
                        </Grid>
                    </>
                }
            </Grid>

            <ThemeProvider theme={theme}>
                <Modal
                    open={open}
                    onClose={handleClose}
                >
                    <Box sx={style}>
                        {
                            payment === ""
                                ?
                                <Typography id="modal-modal-title" variant="h6" component="h2">
                                    <b>Kindly choose Payment method</b>
                                </Typography>
                                :
                                phoneLoad === ""
                                    ?
                                    <Typography id="modal-modal-title" variant="h6" component="h2">
                                        <b>Kindly Give Phone Number</b>
                                    </Typography>
                                    :
                                    null
                        }

                    </Box>
                </Modal>
            </ThemeProvider>
        </Grid>
        </>
    )
}
export default Payment;
