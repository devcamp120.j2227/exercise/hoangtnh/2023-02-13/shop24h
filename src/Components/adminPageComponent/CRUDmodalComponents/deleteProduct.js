import { useDispatch, useSelector } from "react-redux"
import { Dialog, Typography, Grid, DialogTitle, DialogContent, DialogActions, Button } from "@mui/material";
import { callApiDeleteProduct, setStatusDeleteProduct } from "../../../actions/account.action";
import CustomizedSnackbars from "./toast";
export const DeleteProduct = () =>{
    const dispatch = useDispatch();
    const {productInfo, statusDeleteProduct, toast} = useSelector((reduxData)=>
        reduxData.accountReducer
    )
    const onBtnDeleteHandler = () =>{
        dispatch(callApiDeleteProduct(productInfo._id));
        dispatch(setStatusDeleteProduct(false));
    }
    const handleClose = () =>{
        dispatch(setStatusDeleteProduct(false));
    }
    return(
        <>
        {Object.keys(productInfo).length === 0 ? null :
            <Dialog open= {statusDeleteProduct} onClose={handleClose} scroll= "paper">  
                    <DialogTitle>
                        <Typography textAlign="center" variant="h6" component="h2">
                            DELETE PRODUCT
                        </Typography>
                    </DialogTitle>
                    <DialogContent dividers='paper'>
                        <Typography sx ={{mt:2}}>
                            <Grid container spacing={2}>
                                <Grid item >
                                    <p>Confirm to Delete this product: {productInfo.Name}</p>
                                </Grid>
                            </Grid>
                        </Typography>
                    </DialogContent>
                    <DialogActions>
                        <Button  onClick={handleClose} variant="outlined">Close</Button>
                        <Button onClick={onBtnDeleteHandler} variant="contained" style={{backgroundColor:"#d41616"}}>Delete Product</Button>
                    </DialogActions>
            </Dialog>
            }
            {toast.open? <CustomizedSnackbars/>: null}

        </>
    )
}