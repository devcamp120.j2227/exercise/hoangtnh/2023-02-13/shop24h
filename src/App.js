import "bootstrap/dist/css/bootstrap.min.css"
import "./App.css";
import { Route, Routes } from "react-router-dom"
import PersistentDrawerLeft from "./Components/adminPageComponent/pageComponent/drawer";
import routes, { adminRouterList, loginAdminList } from "./routes";
import ShopLayout from "./layout/shopLayout";
import PrivateRoutes from "./Components/privateRouter";

function App() {
  return (
    <div>
          <Routes>
            {routes.map((router, index)=>{
                if(router.path) {
                    return <Route key = {index}  exact path={router.path} element={<ShopLayout>{router.element}</ShopLayout>}>

                    </Route>
                }
            })}
            <Route element ={<PrivateRoutes/>}>
              {adminRouterList.map((router, index)=>{
                  if(router.path) {
                      return <Route key = {index}  exact path={router.path} element={<PersistentDrawerLeft>{router.element}</PersistentDrawerLeft>}>

                      </Route>
                  }
              })}
            </Route>
             {loginAdminList.map((router, index)=>{
                if(router.path) {
                    return <Route key = {index}  exact path={router.path} element={router.element}>

                    </Route>
                }
            })}
        </Routes>
    </div>
  );
}

export default App;
