import { ThemeProvider } from "@mui/material";
import { theme } from "../Components/userProfileComponent/userProfile";
import FooterShop24h from "../views/Footer";
import HeaderShop24h from "../views/Header";

const ShopLayout = ({children})=>{
    
    return (
        <ThemeProvider theme={theme} >
            <div className="home-page">
                <HeaderShop24h/>
                    {children}
                <FooterShop24h/>
            </div>
        </ThemeProvider>
    )
}
export default ShopLayout;