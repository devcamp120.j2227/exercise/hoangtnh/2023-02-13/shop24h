import { Container, Grid, TableContainer, Table, TableRow, TableCell, TableBody, Paper, TableHead,Button, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import ClearIcon from '@mui/icons-material/Clear';
import { addProductToCartHandler, onBtnMinusHandler, onBtnPlusHandler, onBtnRemoveProduct, setPage } from "../../actions/product.action";
import { Link, useNavigate } from "react-router-dom";
import { ThemeProvider, createTheme } from '@mui/material/styles'; 
import avatarDefault from "../../assets/default avatar/png-transparent-default-avatar-thumbnail.png"
import { useState } from "react";
import { useEffect } from "react";

const theme = createTheme({
  typography: {
    allVariants: {
      fontFamily: 'Comfortaa',
      textTransform: 'none',
      fontSize: 16,
    },
  },
});
const fetchApi = async(url, body) =>{
    const response = await fetch(url,body);
    const data = await response.json();
    
    return data;
}
const CheckProductAndPayment = () =>{
    const {cartBag, user} = useSelector((reduxData) =>
        reduxData.productReducer
    );
    const {accountUser} =  useSelector((reduxData)=>
        reduxData.accountReducer
    );
    const [uid, setUid] = useState("");
    const [email, setEmail] = useState("");
    const [photoURL, setPhotoUrl] = useState("");
    const [displayName, setDisplayName] = useState("")
    useEffect(()=>{
        if(accountUser.data !== undefined){
            setUid(accountUser.data.exitUser._id)
            setEmail(accountUser.data.exitUser.Email);
            setPhotoUrl(avatarDefault)
            setDisplayName(accountUser.data.exitUser.Username)
        }
        if(user!== null){
            setUid(user.uid)
            setEmail(user.email)
            setPhotoUrl(user.photoURL)
            setDisplayName(user.displayName)
        }
    }, [accountUser, user, uid])
    const navigate = useNavigate();
    const dispatch = useDispatch();
        //hàm xử lý nút tăng sản phẩm
        const onBtnPlus = (e) =>{
            dispatch(onBtnPlusHandler(e.Product._id));
        };
        //hàm xử lý nút giảm sản phẩm
        const onBtnMinus = (e) =>{
            dispatch(onBtnMinusHandler(e.Product._id));
        };
        //hàm xử lý nút xóa sản phẩm
        const onBtnDelProduct = (e) =>{
            dispatch(onBtnRemoveProduct(e.Product._id));
        }
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        const total = numberWithCommas(cartBag.reduce((sumary, item)=> sumary+ item.Product.PromotionPrice*item.quantity, 0));

        const onBtnCheckOut = () =>{
            var totalCheckout = cartBag.reduce((sumary, item)=> sumary+ item.Product.PromotionPrice*item.quantity, 0)
            if(totalCheckout > 0){
                navigate("/Payment");
                dispatch(setPage("address"));
            }
        }
    return(
        <>
            <Container >
                <div className="check-again-desktop">
                <Grid container direction="row" className="check-page"
                        justifyContent="flex-start"
                        alignItems="center"
                        marginBottom={5}
                        >
                    <Grid item margin={1}>
                        <b>COSY-HOME</b>
                    </Grid>
                    <Grid item>
                        |
                    </Grid>
                    <Grid item margin={1}>
                        SHOPPING CART
                    </Grid>
                </Grid>
                {user !== null  || accountUser.length !== 0 ?
                <ThemeProvider theme={theme}>
                    <TableContainer component={Paper} className="mb-4">
                        <Table aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="left"><b>Products</b></TableCell>
                                    <TableCell align="left" ><b>Price</b></TableCell>
                                    <TableCell align="center" ><b>Quantity</b></TableCell>
                                    <TableCell align="left" ><b>Total</b></TableCell>
                                    <TableCell align="left" ></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                            {cartBag.map((value, index) => (
                                <TableRow
                                key={index}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                <TableCell component="th" scope="row" align="left"style={{width:"80%"}}><img src={value.Product.ImageUrl} style={{height:"auto", width:"100px"}}/> &nbsp;&nbsp;{value.Product.Name}</TableCell>
                                <TableCell align="left"style={{width:"100%",paddingRight:0  }}>$ {value.Product.PromotionPrice}</TableCell>
                                <TableCell align="center" style={{width:"100%"}}>
                                    <Grid container direction="row"
                                        justifyContent="center"
                                        alignItems="center" style={{width:"max-content"}}>
                                            <Grid item>
                                                <button style={{margin:"5px",padding:"2px 11px", borderRadius:"50%", border:"0px", backgroundColor:"#d1c286", color:"white"}}
                                                    onClick={() => onBtnMinus(value)}>-</button>
                                            </Grid>
                                            <Grid item>
                                            { value.quantity <= 0? 1:value.quantity}
                                            </Grid>
                                            <Grid item>
                                                <button style={{margin:"5px",padding:"2px 9px", borderRadius:"50%", border:"0px", backgroundColor:"#d1c286", color:"white"}}
                                                    onClick={() => onBtnPlus(value)}>+</button>
                                            </Grid>
                                        </Grid>      
                                </TableCell>
                                <TableCell align="left" style={{width:"100%",paddingRight:0}}> 
                                <Grid container direction="row" sx={{width:"100px"}}>$ {numberWithCommas(value.Product.PromotionPrice * value.quantity)}</Grid></TableCell>
                                <TableCell align="left"><ClearIcon type="button" onClick= {()=> onBtnDelProduct(value)}/></TableCell>
                                </TableRow>
                            ))} 
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <Grid>
                        <Button style={{backgroundColor:"gray", color: "white",marginBottom:"10px"}} onClick={()=>navigate("/Products")}> Continue Shopping</Button>
                    </Grid>
                    {
                    cartBag.length ===0? 
                        <>
                            <Grid textAlign="center" margin={"15%"} style={{ color: "#d1c286" }}>Your Cart Bag is Emty.</Grid>
                        </>:
                    <Grid container  style={{backgroundColor:"whiteSmoke", padding:10, width:"60%", margin:"auto", marginBottom:20}}
                            direction="column" md={12} sm={12} lg={12} xs={12}>
                        <Grid item  textAlign="center" marginBottom={5}>
                            <h4>Cash total</h4>
                        </Grid>
                        <Grid container sx={{width:"100%"}}
                            direction="row"
                            marginBottom={5}>
                            <Grid item textAlign="center" xs={6}>
                                <b>Customer:</b>
                            </Grid>
                            <Grid item textAlign="center"xs={6}>
                                <b style={{color:"#d1c286"}}> {displayName}</b>
                            </Grid>
                        </Grid>
                        <Grid container sx={{width:"100%"}}
                            direction="row"
                            marginBottom={5}>
                            <Grid item textAlign="center" xs={6}>
                                <b>Total:</b>
                            </Grid>
                            <Grid item textAlign="center"xs={6}>
                                <b style={{color:"#d1c286"}}>$ {total}</b>
                            </Grid>
                        </Grid>
                        <Grid container
                            direction="row"
                            justifyContent="center" >
                            <Button style={{backgroundColor:"#7fad39", color:"white", padding:"10px"}} onClick={onBtnCheckOut}><b>Process To Check out</b></Button>
                        </Grid>
                    </Grid>}
                    </ThemeProvider>
            : 
            
            <Grid textAlign="center" margin={"15%"} style={{color:"#d1c286"}}>You need to <Link to="/Login">sign in</Link> for completing check out your cart</Grid>}
                </div>
            </Container>
            <Container >
                <div className="check-again-mobile">
                <Grid container direction="row" className="check-page"
                    justifyContent="flex-start"
                    alignItems="center"
                    marginBottom={5}
                >
                    <Grid item margin={1}>
                        <b>COSY-HOME</b>
                    </Grid>
                    <Grid item>
                        |
                    </Grid>
                    <Grid item margin={1}>
                        SHOPPING CART
                    </Grid>
                </Grid>
                {user !== null || accountUser.length !== 0 ?
                    <ThemeProvider theme={theme}>
                        <TableContainer component={Paper} className="mb-4" >
                            <Table aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="center"><b>Products</b></TableCell>
                                        <TableCell align="left" ><b>Price</b></TableCell>
                                        <TableCell align="left" ><b>Quantity</b></TableCell>
                                        <TableCell align="left" style={{padding:0}}></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {cartBag.map((value, index) => (
                                        <TableRow
                                            key={index}
                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                            <TableCell component="th" scope="row" align="center" style={{ width: "80%" }}><img src={value.Product.ImageUrl} style={{ height: "auto", width: "100px" }} /> &nbsp;&nbsp;{value.Product.Name}</TableCell>
                                            <TableCell align="left" style={{ width: "100%", paddingRight: 0 }}>$ {value.Product.PromotionPrice}</TableCell>
                                            <TableCell align="left" style={{ width: "100%", paddingRight: 10 }}>
                                                <Grid container direction="row"
                                                    justifyContent="center"
                                                    alignItems="center" style={{ width: "max-content" }}>
                                                    <Grid item>
                                                        <button style={{ margin: "5px", padding: "2px 11px", borderRadius: "50%", border: "0px", backgroundColor: "#d1c286", color: "white" }}
                                                            onClick={() => onBtnMinus(value)}>-</button>
                                                    </Grid>
                                                    <Grid item>
                                                        {value.quantity <= 0 ? 1 : value.quantity}
                                                    </Grid>
                                                    <Grid item>
                                                        <button style={{ margin: "5px", padding: "2px 9px", borderRadius: "50%", border: "0px", backgroundColor: "#d1c286", color: "white" }}
                                                            onClick={() => onBtnPlus(value)}>+</button>
                                                    </Grid>
                                                </Grid>
                                            </TableCell>
                                            <TableCell align="left" style={{padding:0}}><ClearIcon type="button" onClick={() => onBtnDelProduct(value)} /></TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                        <Grid>
                            <Button style={{ backgroundColor: "gray", color: "white", marginBottom: "10px" }} onClick={() => navigate("/Products")}> Continue Shopping</Button>
                        </Grid>
                        {
                            cartBag.length === 0 ?
                                <>
                                    <Grid textAlign="center" margin={"15%"} style={{ color: "#d1c286" }}>Your Cart Bag is Emty.</Grid>
                                </> :
                                <Grid container style={{ backgroundColor: "whiteSmoke", padding: 10, width: "100%", margin: "auto", marginBottom: 20 }}
                                    direction="column" md={12} sm={12} lg={12} xs={12}>
                                    <Grid item textAlign="center" marginBottom={5}>
                                        <h4>Cash total</h4>
                                    </Grid>
                                    <Grid container sx={{ width: "100%" }}
                                        direction="row"
                                        marginBottom={5}>
                                        <Grid item textAlign="center" xs={6}>
                                            <b>Customer:</b>
                                        </Grid>
                                        <Grid item textAlign="center" xs={6}>
                                            <b style={{ color: "#d1c286" }}> {displayName}</b>
                                        </Grid>
                                    </Grid>
                                    <Grid container sx={{ width: "100%" }}
                                        direction="row"
                                        marginBottom={5}>
                                        <Grid item textAlign="center" xs={6}>
                                            <b>Total:</b>
                                        </Grid>
                                        <Grid item textAlign="center" xs={6}>
                                            <b style={{ color: "#d1c286" }}>$ {total}</b>
                                        </Grid>
                                    </Grid>
                                    <Grid container
                                        direction="row"
                                        justifyContent="center" >
                                        <Button style={{ backgroundColor: "#7fad39", color: "white", padding: "10px" }} onClick={onBtnCheckOut}><b>Process To Check out</b></Button>
                                    </Grid>
                                </Grid>}
                    </ThemeProvider>
                    :

                    <Grid textAlign="center" margin={"15%"} style={{ color: "#d1c286" }}>You need to <Link to="/Login">sign in</Link> for completing check out your cart</Grid>}
                </div>
            </Container>
        </>
    )
}
export default CheckProductAndPayment;
