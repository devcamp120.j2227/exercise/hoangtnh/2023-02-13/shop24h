import img1 from "../../../assets/slideImg/slide1.webp"
import img2 from "../../../assets/slideImg/slide2.webp"
import img3 from "../../../assets/slideImg/slide5.jpg"
import img4 from "../../../assets/slideImg/slide6.jpg"
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import { Row } from "reactstrap";
const SliderCaroul = () =>{
  const settings = {
    dots: true,
    fade: true,
    autoplay: true,
    speed: 2000,
    autoplaySpeed: 3000,
    dotsClass: "slick-dots slick-thumb",
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };
  return (
      <div>
        <Row className="text-center mt-2" style={{color:"#d1c286"}}>
          <h1 style={{margin:"0px", padding: 0}}>COSY-HOME</h1>
          <div className="d-flex justify-content-center">
              <hr style={{width:"250px",marginTop:"-2px", border:"1px solid "}}/>
          </div>
        </Row>
          <Slider {...settings}>
            <div>
              <img className="ImgCasroul" src={img1} />
              <div className="text-img">
                <h1>PRODUCT</h1>
                <p>Incididunt ut labore et dolore magna aliqua quis ipsum suspendisse ultrices gravida. Risus commodo viverra</p>
              </div>
            </div>
            <div >
              <img className="ImgCasroul" src={img4} />
              <div className="text-img">
                  <h1>SERVICES</h1>
                  <p>Incididunt ut labore et dolore magna aliqua quis ipsum suspendisse ultrices gravida. Risus commodo viverra</p>
              </div>
            </div>
            <div>
              <img src={img2} className="ImgCasroul"/>
              <div className="text-img">
                  <h1>VINTAGE</h1>
                  <p>Incididunt ut labore et dolore magna aliqua quis ipsum suspendisse ultrices gravida. Risus commodo viverra</p>
              </div>
            </div>
            <div>
              <img src={img3} className="ImgCasroul"/>
              <div className="text-img">
                  <h1>LUXURY</h1>
                  <p>Incididunt ut labore et dolore magna aliqua quis ipsum suspendisse ultrices gravida. Risus commodo viverra</p>
              </div>
            </div>
          </Slider>
      </div>
  );
}
export default SliderCaroul;