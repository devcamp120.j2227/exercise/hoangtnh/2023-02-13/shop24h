import PersistentDrawerLeft from "./pageComponent/drawer"
import { theme } from "../userProfileComponent/userProfile";
import { ThemeProvider } from "@mui/material";
import LoginAdmin from "./loginAdminComponent/loginAdmin";
import { useSelector } from "react-redux";
import { useEffect } from "react";

const AdminPage = () =>{
    const {account, refToken} = useSelector((reduxData)=>
        reduxData.accountReducer
    )
    useEffect(()=>{

    },[refToken])
    return(
        <ThemeProvider theme={theme}>
            {refToken !==""?
            <PersistentDrawerLeft/>
            :<LoginAdmin/>
            }
        </ThemeProvider>
    )
}
export default AdminPage;