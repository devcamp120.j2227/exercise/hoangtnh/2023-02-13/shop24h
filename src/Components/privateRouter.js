import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";

const PrivateRoutes =() =>{
    const { refToken} = useSelector((reduxData)=>
        reduxData.accountReducer
    )
    
    return refToken ===""?<Navigate to="/LoginAdmin"/>:<Outlet/>
}
export default PrivateRoutes;