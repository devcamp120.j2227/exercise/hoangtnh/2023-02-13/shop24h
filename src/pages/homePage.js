import AboutUs from "../Components/bodyComponent/bodyContent/aboutUs"
import SliderCaroul from "../Components/bodyComponent/bodyContent/Casouline"
import Categories from "../Components/bodyComponent/bodyContent/Categories"
import LastestProduct from "../Components/bodyComponent/bodyContent/LastestProduct"

const HomePageShop24h = () =>{

    return (
    <div style ={{overflowX: "hidden"}} className="content-page">
            <SliderCaroul/>
        <div className="div-lastestProduct">
            <LastestProduct/>
            <Categories/>
            <AboutUs/>
        </div>
    </div>
    )
}
export default HomePageShop24h;