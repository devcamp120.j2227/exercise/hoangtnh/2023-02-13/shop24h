import * as React from 'react';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import { useDispatch, useSelector } from 'react-redux';
import { openToast } from '../../../actions/account.action';
import { openToastAddCart } from '../../../actions/product.action';
import { ThemeProvider } from '@mui/material';
import { theme } from '../../userProfileComponent/userProfile';

const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});
export default function CustomizedSnackbars() {
    const dispatch = useDispatch();
    const {toast} = useSelector((reduxData)=>
        reduxData.accountReducer
    )
    const {toastAddCart} = useSelector((reduxData)=>
        reduxData.productReducer
    )
    const handleClose = (event, reason) => {
      if (reason === 'clickaway') {
        return;
      }
        dispatch(openToast(toast.open = false));
    };
    // const handleCloseAddCart = (event, reason) => {
    //   if (reason === 'clickaway') {
    //     return;
    //   }
    //   dispatch(openToastAddCart(toastAddCart.open = false))
    // };
    return (
      <ThemeProvider theme={theme}>
        {toast.open ?
        <Snackbar open={toast.open} autoHideDuration={6000} onClose={handleClose}>
            <Alert onClose={handleClose} severity={toast.type} sx={{ width: '100%' }}>
                {toast.message}
            </Alert>
        </Snackbar>
        // : toastAddCart.open?
        // <Snackbar open={toastAddCart.open} autoHideDuration={6000} onClose={handleCloseAddCart}>
        //     <Alert onClose={handleCloseAddCart} severity={toastAddCart.type} sx={{ width: '100%' }}>
        //         {toastAddCart.message}
        //     </Alert>
        // </Snackbar>
        :null}
      </ThemeProvider>
    );
  }
