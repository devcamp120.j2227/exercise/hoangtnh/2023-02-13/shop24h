import { Grid, Button, Alert,Snackbar  } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Row, Col, Input, FormGroup, Card, CardBody } from 'reactstrap';
import getAccount from "../../../actions/account.action";
const LoginAdmin = () =>{
    const [username, getUsername] = useState("");
    const [password, getPassword] = useState("");
    const [openAlert, setOpenAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState("");

    const dispatch = useDispatch();
    
    const {wrongAccount} = useSelector((reduxData)=>
        reduxData.accountReducer
    )
    const navigate = useNavigate()
    //load lại giá trị khi reducer thay đổi
   useEffect(()=>{
    if(wrongAccount ==="username is incorrect"){
        setOpenAlert(true);
        setAlertMessage(wrongAccount)
    }
    if(wrongAccount ==="Password is incorrect"){
        setOpenAlert(true);
        setAlertMessage(wrongAccount)
    }
    if(wrongAccount ===""){
        setOpenAlert(false);
    }
   }, [wrongAccount])
   //nút login được bấm
    const onBtnLogin = ()=>{
        if(username ===""){
            setOpenAlert(true);
            setAlertMessage("Please fill username")
        }
        if(username !=="" && password ===""){
            setOpenAlert(true);
            setAlertMessage("Please fill password")
        }
            //truyền call back function qua action redux
            dispatch(getAccount({username: username, password: password}, ()=> navigate("/Administrator")));
    }
    //đóng mở alert
    const onCloseHandler = (event, reason) =>{
        if (reason === 'clickaway') {
            return;
          }
        setOpenAlert(false);
    }
    
    return (
        <Grid container md={12} sm={12} lg={12} xs={12} className="admin-login-page">
            <Grid container justifyContent="flex-end" 
                    direction="row" style={{height:"10px"}}> 
                <Snackbar open={openAlert} autoHideDuration={6000} onClose={onCloseHandler}>
                    <Alert onClose={onCloseHandler} severity="error" sx={{ width: '100%' }}>
                        {alertMessage}
                    </Alert> 
                </Snackbar>
            </Grid>
            <Grid container md={12} sm={12} lg={12} xs={12}
                justifyContent="center"
                alignItems="flex-start"
                        >
                <Card style={{ width:"50%", border:"none", backgroundColor:"transparent"}} className="admin-login-form">
                    <CardBody className="d-flex justify-content-center"  style={{margin:"3%"}}>
                        <Row >
                            <Col sm={12} className='text-center'>
                                <h4 style={{color:"white"}}>Log In As Admin</h4>
                            </Col>
                            <Col sm={12} className='d-flex flex-column align-items-center'>
                                <FormGroup style={{width:"80%"}} sm={12}>
                                    <Input
                                    name="username"
                                    placeholder="Username"
                                    style={{borderRadius:"50px"}}
                                    onChange={(e)=> getUsername(e.target.value)}
                                    />
                                </FormGroup>
                                <FormGroup style={{width:"80%"}}>
                                    <Input
                                    name="password"
                                    placeholder="Password"
                                    type="password"
                                    style={{borderRadius:"50px"}}
                                    onChange={(e)=> getPassword(e.target.value)}
                                    />
                                </FormGroup>
                                <Button style={{ width:"80%",borderRadius:"50px", backgroundColor:"#d1c286", color:"white"}}
                                onClick={onBtnLogin}>
                                    Log In  
                                </Button>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
                
            </Grid>
        </Grid>
    )
}
export default LoginAdmin;