// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import {getAuth} from "firebase/auth";
import {getStorage} from "firebase/storage"
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDOCHo9Jmx_4WPb2ALJOZnYcVUwKQM9rYg",
  authDomain: "task-530.firebaseapp.com",
  projectId: "task-530",
  storageBucket: "task-530.appspot.com",
  messagingSenderId: "870293941404",
  appId: "1:870293941404:web:afe20b57d9acc5fef45159"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth(app);
export const storage = getStorage(app)
export default auth