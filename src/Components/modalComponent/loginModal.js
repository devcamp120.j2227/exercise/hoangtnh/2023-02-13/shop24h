import { useState, useEffect } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col, Input, FormGroup, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import '@fortawesome/fontawesome-free/css/all.min.css';
//import các lệnh đăng nhập gg
import auth from '../../firebase';
import { GoogleAuthProvider, signInWithPopup, signOut, onAuthStateChanged } from 'firebase/auth';
import { useDispatch, useSelector } from 'react-redux';
import { getProfileUser, loginUserInformation, setPage } from '../../actions/product.action';
import { Link, useNavigate } from 'react-router-dom';
import { Typography } from '@mui/material';
import defaultAvatar from "../../assets/default avatar/png-transparent-default-avatar-thumbnail.png"
import { loginAccount, logOutCustomer, openToast } from '../../actions/account.action';
import CustomizedSnackbars from '../adminPageComponent/CRUDmodalComponents/toast';
//đăng nhập bằng tài khoản gg trên react
const provider = new GoogleAuthProvider();
function LoginModal(args) {
    //call dispatch để truyền dữ liệu user vào redux
    const dispatch = useDispatch();
    //khởi tạo các trạng thái ban đầu
    const [open, setOpen] = useState(false);
    const [dropdownOpen, setDropdownOpen] = useState(false);
    const [loginAs, setLoginAs] = useState("Google")
    //thao tác khi bấm vào avatar
    const toggleAvatar = () => setDropdownOpen(!dropdownOpen);
    const toggle = () => setOpen(!open);
    const { loginAsCustomer, accountUser, toast } = useSelector((reduxData) =>
        reduxData.accountReducer
    )
    const navigate = useNavigate();

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const account = {
        username: username,
        password: password
    }
    //login gg khởi tạo giá trị user ban đầu
    const [user, setUser] = useState(null);
    const [customer, setCustomer] = useState(null)
    
    //xử lý nút đăng nhập bằng tài khoản gg
    const onLogInButton = () => {
        signInWithPopup(auth, provider)
            .then((result) => {
                setUser(result.user)
                setLoginAs("Google")
            })
            .catch((error) => {
                console.error(error)
            });
        setOpen(!open)
        //tạo tài khoản> đăng nhập > "userAccount" > setItem.localstorage 
    }
    //xem profile cá nhân
    const editProfile = () => {
        navigate("/User");
        dispatch(setPage("profile"))
    }
    
    const onLoginHandler = () => {
        setLoginAs("account");
        dispatch(loginAccount(account, () => navigate("/")))
        if(loginAsCustomer ==="true password, Login successful") {
            setOpen(!open);
        }
    }
    //log out 
    const logoutGoogle = () => {
        if (loginAs === "Google") {
            signOut(auth)
                .then(() => {
                    setUser(null);
                    //dispatch(getProfileUser(user))
                    navigate("/");
                    window.location.reload();
                    setLoginAs("Google")
                })
                .catch((error) => {
                    console.error(error);
                })
        }
        if (loginAs === "account") {
            setOpen(false)
            setUsername("");
            setPassword("")
            setCustomer(null)
            dispatch(logOutCustomer())
            localStorage.removeItem("userAccount");
            setLoginAs("Google");
            navigate("/");
            window.location.reload()
        }
    }


    //giữ lại trạng thái user đăng nhập khi reload trang
    useEffect(() => {
        onAuthStateChanged(auth, (result) => {
            setUser(result);
            //truyền user vào redux để thao tác trên check cart
            dispatch(loginUserInformation(result));
        })
    }, [user, auth, accountUser]);
    useEffect(() => {
        if(loginAsCustomer ==="true password, Login successful"){
            setCustomer(accountUser.data.exitUser);
            setLoginAs("account")
        }
    }, [ toast, accountUser, customer,loginAs])
    return (
        <>
        <CustomizedSnackbars/>
            {user ?
                <Row>
                    <Col sm={3} className="d-flex align-items-center">
                        <Dropdown nav isOpen={dropdownOpen} toggle={toggleAvatar} style={{ listStyleType: "none" }}>
                            <DropdownToggle nav >
                                <img src={user.photoURL} alt="no-referrer" width={30} style={{ borderRadius: "50%" }} />
                            </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem>
                                    <Typography style={{ backgroundColor: "#d1c286", padding: 10, borderRadius: "10px", color: "white", fontFamily: "Comfortaa" }} onClick={editProfile}>Profile <i className="fa-regular fa-user"></i></Typography>
                                </DropdownItem>
                                <DropdownItem>
                                    <Typography style={{ backgroundColor: "#d1c286", padding: 10, borderRadius: "10px", color: "white", fontFamily: "Comfortaa" }} onClick={logoutGoogle} >Log out</Typography>
                                </DropdownItem>
                            </DropdownMenu>
                        </Dropdown>
                    </Col>
                </Row>
                : customer ?
                    <Row>
                        <Col sm={3} className="d-flex align-items-center">
                            <Dropdown nav isOpen={dropdownOpen} toggle={toggleAvatar} style={{ listStyleType: "none" }}>
                                <DropdownToggle nav >
                                    <img src={defaultAvatar} alt="no-referrer" width={25} style={{ borderRadius: "50%" }} />
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem>
                                        <Typography style={{ backgroundColor: "#d1c286", padding: 10, borderRadius: "10px", color: "white", fontFamily: "Comfortaa" }} onClick={editProfile}>Profile <i className="fa-regular fa-user"></i></Typography>
                                    </DropdownItem>
                                    <DropdownItem>
                                        <Typography style={{ backgroundColor: "#d1c286", padding: 10, borderRadius: "10px", color: "white", fontFamily: "Comfortaa" }} onClick={logoutGoogle} >Log out</Typography>
                                    </DropdownItem>
                                </DropdownMenu>
                            </Dropdown>
                        </Col>
                    </Row> :
                    <div>
                        <i className="fa-solid fa-user" onClick={toggle}></i>
                        <Modal isOpen={open} toggle={toggle} {...args}>
                            <ModalHeader toggle={toggle} style={{ color: "#d1c286" }}>Sign In</ModalHeader>
                            <ModalBody >
                                <Row >
                                    <Col className='d-flex justify-content-center' sm={12}>
                                        <Button color='danger' style={{ width: "80%", borderRadius: "50px" }}
                                            onClick={onLogInButton}>
                                            <i className="fa-brands fa-google" /> Sign In with <b>Google</b>
                                        </Button>
                                    </Col>
                                    <Col className='d-flex justify-content-center' sm={12}>
                                        <div><hr style={{ width: "150px", border: "1px solid black" }} /></div>
                                    </Col>
                                    <Col sm={12} className='d-flex flex-column align-items-center'>
                                        <FormGroup style={{ width: "80%" }} sm={12}>
                                            <Input
                                                name="username"
                                                placeholder="Username"
                                                style={{ borderRadius: "50px" }}
                                                onChange={(e)=> setUsername(e.target.value)}
                                            />
                                        </FormGroup>
                                        <FormGroup style={{ width: "80%" }}>
                                            <Input
                                                name="password"
                                                placeholder="Password"
                                                type="password"
                                                style={{ borderRadius: "50px" }}
                                                onChange={(e)=> setPassword(e.target.value)}
                                            />
                                        </FormGroup>
                                        <Button color='success' style={{ width: "80%", borderRadius: "50px" }} onClick={onLoginHandler}>
                                            Sign In
                                        </Button>
                                    </Col>
                                </Row>
                            </ModalBody>
                            <ModalFooter>
                                <Col className='text-center'>
                                    <p> Don't have an account? </p>
                                    <Link to="/Account" onClick={() => setOpen(!open)} style={{ color: "green" }}>Sign up here</Link>
                                </Col>
                            </ModalFooter>
                        </Modal>
                    </div>
            }
        </>
    );
}

export default LoginModal;
