import { Breadcrumb, BreadcrumbItem } from "reactstrap"
import { Link, useParams } from "react-router-dom"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { callApiEditProduct, getProductInfo, getStatusDeleteImg, openToast, getImgUrlHandler } from "../../../actions/account.action";
import { Grid, Typography, TextField, TextareaAutosize, Button, MenuItem } from "@mui/material";
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import CancelIcon from '@mui/icons-material/Cancel';
import { storage } from "../../../firebase";
import { ref, uploadBytes, listAll, getDownloadURL } from "firebase/storage";
import { v4 } from "uuid"
import CustomizedSnackbars from "../CRUDmodalComponents/toast";
import AddIcon from '@mui/icons-material/Add';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { DeleteModalImage } from "../CRUDmodalComponents/deleteImageModal";
const ProductInfor = () =>{
    const dispatch = useDispatch()
    const {productId} = useParams();
    const {productInfo, toast} = useSelector((reduxData)=>
        reduxData.accountReducer
    );
    const [productDescription , getProductDescription] = useState ("") 
    const [productName, getProductName] = useState("");
    const [productType, getProductType] = useState("");
    const [description, getDescription] = useState ("");
    const [buyPrice, getBuyPrice] = useState(0);
    const [promotionPrice, getPromotionPrice] = useState(0);
    const [imgUrl, getImgUrl] = useState(null);
    const [imgList, getImgList] = useState([]);
    const [imgSelected, getImgSelected] = useState(null)
    const [deliveryFee, getDeliveryFee] = useState("");
    const [deliverTime, getDeliverTime] = useState("");
    const [warrantyTime, getWarrantyTime] = useState("");
    const [amount, getAmount]  = useState(0)
    console.log(amount)
    const [productDescriptionEdit , editProductDescription] = useState(false) 
    const [productNameEdit, editProductName] = useState(false);
    const [productTypeEdit, editProductType] = useState(false);
    const [descriptionEdit, editDescription] = useState (false);
    const [buyPriceEdit, editBuyPrice] = useState(false);
    const [promotionPriceEdit, editPromotionPrice] = useState(false);
    const [imgUrlEdit, editImgUrl] = useState(false);
    const [deliveryFeeEdit, editDeliveryFee] = useState(false);
    const [deliverTimeEdit, editDeliverTime] = useState(false);
    const [warrantyTimeEdit, editWarrantyTime] = useState(false);
    const [amountEdit, editProductAmount] = useState(false)

    const editProduct = {
                Name: productName,
                Type: productType,
                Description: description,
                BuyPrice: buyPrice,
                PromotionPrice : promotionPrice,
                ProductDescription: productDescription,
                ImageUrl: imgSelected,
                DeliveryFee: deliveryFee,
                DeliverTime: deliverTime,
                Warranty: warrantyTime,
                Amount: amount
            }

    const imgListRef = ref(storage, "imageProduct/")
    useEffect(()=>{
        getProductName(productInfo.Name)
        getProductDescription(productInfo.ProductDescription)
        getProductType(productInfo.Type)
        getDescription(productInfo.Description)
        getBuyPrice(productInfo.BuyPrice)
        getPromotionPrice(productInfo.PromotionPrice)
        getImgSelected(productInfo.ImageUrl)
        getDeliveryFee(productInfo.DeliveryFee)
        getDeliverTime(productInfo.DeliverTime)
        getWarrantyTime(productInfo.Warranty)
        getAmount(productInfo.Amount)
    }, [productInfo])
    useEffect(()=>{
        dispatch(getProductInfo(productId));

        const fetchImages = async () => {
            const result = await listAll(imgListRef);
            const urlPromises = result.items.map((imageRef) =>{
              return getDownloadURL(imageRef)
            });
      
            return Promise.all(urlPromises);
          };
      
          const loadImages = async () => {
            const urls = await fetchImages();
            getImgList(urls);
          };
          loadImages();
         
    }, [toast, productDescriptionEdit, productId])

    //xử lý thông tin link ảnh
    const onImageUrlHandler = ()=>{
        if(imgUrl === null) {
            return false;
        }
        else{
            const imgRef = ref(storage, `imageProduct/${imgUrl.name + v4()}`);
            uploadBytes(imgRef, imgUrl)
                .then(() =>{
                    dispatch(openToast({
                        open: true,
                        message: "upload image successfull",
                        type: "success"
                    }))
                })
                .then(()=>{
                    getImgUrl(null)
                })
        }
    }
    
    //nút save update product
    const onBtnSaveProduct =()=>{
       dispatch(callApiEditProduct(editProduct, productId));
    }
    const deleteImageHandler =(url)=>{
        dispatch(getImgUrlHandler(url));
        dispatch(getStatusDeleteImg(true))
    }
    
    return (
        <>
        <DeleteModalImage/>
            {Object.keys(productInfo).length === 0 ? null :
                <>
                {toast.open? <CustomizedSnackbars/>: null}
                    <Breadcrumb>
                        <BreadcrumbItem >
                            <Link to="/Administrator/Staff" style={{ textDecoration: "none" }}>Products</Link>
                        </BreadcrumbItem>
                        <BreadcrumbItem >
                            <Link>Detail product</Link>
                        </BreadcrumbItem>
                    </Breadcrumb>
                    <Grid container direction="column"
                        justifyContent="flex-start"
                        alignItems="flex-start"
                        style={{ backgroundColor: "whiteSmoke", padding: 20, borderRadius: "20px" }}
                        md={12} sm={12} lg={12} xs={12} spacing={1} >
                        <Typography style={{ color: "#d1c286" }}>
                            <b><ArrowRightIcon />PRODUCT INFORMATION</b>
                        </Typography>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="center" spacing={2} xs={12} style={{ padding: 10 }}>
                            <Grid item  >
                                <Typography>
                                    Name:
                                </Typography>
                            </Grid>
                            <Grid item >
                                {productNameEdit === false
                                    ? 
                                        <Typography>
                                            <b>{ productName}</b>
                                            <ModeEditIcon type="button" style={{fontSize:"medium",color:"#d1c286",marginTop:"-15"}} onClick={()=> editProductName(true)}/>
                                        </Typography>
                                    :
                                        <>
                                            <TextField variant="outlined" label="Edit product name" defaultValue = {productName} onChange={(e) => getProductName(e.target.value)}/> 
                                            <CancelIcon type="button" style={{fontSize:"medium",marginTop:"-15"}} onClick={()=> {editProductName(false); getProductName(productInfo.Name)}}/>
                                        </>
                                }
                            </Grid>
                            &nbsp; &nbsp; 
                            <Grid item style={{ padding:0}}>
                                 <img src={imgSelected} style={{width:"50px", borderRadius:"48%"}}/>  

                            </Grid>
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                            <Grid item >
                                <Typography>
                                    Type:
                                </Typography>
                            </Grid>
                            <Grid item >
                                {productTypeEdit === false ?
                                    <Typography>
                                        <b>{productType}</b>
                                        <ModeEditIcon type="button" style={{ fontSize: "medium", color: "#d1c286", marginTop: "-15" }} onClick={() => editProductType(true)} />
                                    </Typography>
                                    :
                                    <>
                                        <TextField variant="outlined" label="Edit product type" defaultValue={productType} onChange={(e) => getProductType(e.target.value)} />
                                        <CancelIcon type="button" style={{ fontSize: "medium", marginTop: "-15" }} onClick={() => { editProductType(false); getProductType(productInfo.Type) }} />
                                    </>
                                }
                            </Grid>
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                            <Grid item >
                                <Typography>
                                    Description:
                                </Typography>
                            </Grid>
                            <Grid item >
                                {descriptionEdit === false ?
                                    <Typography>
                                        <b>{description}</b>
                                        <ModeEditIcon type="button" style={{ fontSize: "medium", color: "#d1c286", marginTop: "-15" }} onClick={() => editDescription(true)} />
                                    </Typography>
                                    :
                                    <>
                                        <TextField variant="outlined" label="Edit product description" defaultValue={description} onChange={(e) => getDescription(e.target.value)} />
                                        <CancelIcon type="button" style={{ fontSize: "medium", marginTop: "-15" }} onClick={() => { editDescription(false); getDescription(productInfo.Description) }} />
                                    </>
                                }
                            </Grid>
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                            <Grid item >
                                <Typography>
                                    Price:
                                </Typography>
                            </Grid>
                            <Grid item  >
                                {buyPriceEdit === false ?
                                    <Typography>
                                        <b>$ {buyPrice}</b>
                                        <ModeEditIcon type="button" style={{ fontSize: "medium", color: "#d1c286", marginTop: "-15" }} onClick={() => editBuyPrice(true)} />
                                    </Typography>
                                    :
                                    <>
                                        <TextField variant="outlined" type="number" label="Edit product buy price" defaultValue={buyPrice} onChange={(e) => getBuyPrice(e.target.value)} />
                                        <CancelIcon type="button" style={{ fontSize: "medium", marginTop: "-15" }} onClick={() => { editBuyPrice(false); getBuyPrice(productInfo.BuyPrice) }} />
                                    </>
                                }
                            </Grid>
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                            <Grid item >
                                <Typography>
                                    Promotion price:
                                </Typography>
                            </Grid>
                            <Grid item >
                                {promotionPriceEdit === false ?
                                    <Typography>
                                        <b>$ {promotionPrice}</b>
                                        <ModeEditIcon type="button" style={{ fontSize: "medium", color: "#d1c286", marginTop: "-15" }} onClick={() => editPromotionPrice(true)} />
                                    </Typography>
                                    :
                                    <>
                                        <TextField variant="outlined" type="number" label="Edit product promotion price" defaultValue={promotionPrice} onChange={(e) => getPromotionPrice(e.target.value)} />
                                        <CancelIcon type="button" style={{ fontSize: "medium", marginTop: "-15" }} onClick={() => { editPromotionPrice(false); getPromotionPrice(productInfo.PromotionPrice) }} />
                                    </>
                                }
                            </Grid>
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                            <Grid item >
                                <Typography>
                                    Delivery fee:
                                </Typography>
                            </Grid>
                            <Grid item >
                                {deliveryFeeEdit === false ?
                                    <Typography>
                                        <b>{deliveryFee}</b>
                                        <ModeEditIcon type="button" style={{ fontSize: "medium", color: "#d1c286", marginTop: "-15" }} onClick={() => editDeliveryFee(true)} />
                                    </Typography>
                                    :
                                    <>
                                        <TextField variant="outlined" label="Edit product delivery fee" defaultValue={deliveryFee} onChange={(e) => getDeliveryFee(e.target.value)} />
                                        <CancelIcon type="button" style={{ fontSize: "medium", marginTop: "-15" }} onClick={() => { editDeliveryFee(false); getDeliveryFee(productInfo.DeliveryFee) }} />
                                    </>
                                }
                            </Grid>
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                            <Grid item >
                                <Typography>
                                    Deliver time:
                                </Typography>
                            </Grid>
                            <Grid item >
                                {deliverTimeEdit === false ?
                                    <Typography>
                                        <b>{deliverTime}</b>
                                        <ModeEditIcon type="button" style={{ fontSize: "medium", color: "#d1c286", marginTop: "-15" }} onClick={() => editDeliverTime(true)} />
                                    </Typography>
                                    :
                                    <>
                                        <TextField sx={{width:"300px"}} variant="outlined" label="Edit product delivery fee" defaultValue={deliverTime} onChange={(e) => getDeliverTime(e.target.value)} />
                                        <CancelIcon type="button" style={{ fontSize: "medium", marginTop: "-15" }} onClick={() => { editDeliverTime(false); getDeliverTime(productInfo.DeliverTime) }} />
                                    </>
                                }
                            </Grid>
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                            <Grid item >
                                <Typography>
                                    Warranty time:
                                </Typography>
                            </Grid>
                            <Grid item >
                                {warrantyTimeEdit === false ?
                                    <Typography>
                                        <b>{warrantyTime}</b>
                                        <ModeEditIcon type="button" style={{ fontSize: "medium", color: "#d1c286", marginTop: "-15" }} onClick={() => editWarrantyTime(true)} />
                                    </Typography>
                                    :
                                    <>
                                        <TextField variant="outlined" label="Edit product delivery fee" defaultValue={warrantyTime} onChange={(e) => getWarrantyTime(e.target.value)} />
                                        <CancelIcon type="button" style={{ fontSize: "medium", marginTop: "-15" }} onClick={() => { editWarrantyTime(false); getWarrantyTime(productInfo.Warranty) }} />
                                    </>
                                }
                            </Grid>
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                            <Grid item >
                                <Typography>
                                    Product description:
                                </Typography>
                            </Grid>
                                {productDescriptionEdit === false?
                                <>
                                <Grid item>
                                    <Typography>
                                        <b>{productDescription}</b>
                                        <ModeEditIcon type="button" style={{ fontSize: "medium", color: "#d1c286", marginTop: "-15" }} onClick={() => editProductDescription(true)} />
                                    </Typography>
                                </Grid>
                                </>
                                :
                                <>
                                <Grid item>
                                    <TextareaAutosize
                                        aria-label="minimum height"
                                        minRows={3}
                                        value={productDescription}
                                        style={{ width: "500px", borderRadius:"10px", padding: 5}}
                                        onChange={(e)=> getProductDescription(e.target.value)}
                                        />
                                </Grid>
                                <Grid item>
                                    <CancelIcon type="button" style={{ fontSize: "medium", marginTop: "-15" }} onClick={() => { editProductDescription(false); getProductDescription(productInfo.ProductDescription) }} />
                                </Grid>
                                </>
                                }
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                            <Grid item >
                                <Typography>
                                    Product Amount:
                                </Typography>
                            </Grid>
                                {amountEdit === false?
                                    <>
                                        <Grid item>
                                            <Typography>
                                                <b>{amount ===1? "Available":"Out of Stock"}</b>
                                                <ModeEditIcon type="button" style={{ fontSize: "medium", color: "#d1c286", marginTop: "-15" }} onClick={() => editProductAmount(true)} />
                                            </Typography>
                                        </Grid>
                                        </>
                                        :
                                        <>
                                        <Grid item>
                                            <TextField
                                                select
                                                label="Amount"
                                                value={amount}
                                                sx={{width:"200px"}}
                                                onChange={(e)=> {getAmount(e.target.value)}}
                                            >
                                                <MenuItem value={1}>
                                                    <Typography>Available</Typography>
                                                </MenuItem>
                                                <MenuItem value={0}>
                                                    <Typography>Out of Stock</Typography>
                                                </MenuItem>
                                            </TextField>
                                        </Grid>
                                        <Grid item>
                                            <CancelIcon type="button" style={{ fontSize: "medium", marginTop: "-15" }} onClick={() => { editProductAmount(false); getAmount(productInfo.Amount) }} />
                                        </Grid>
                                    </>
                                }
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-start"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                            <Grid item >
                                <Typography>
                                    Image product:
                                </Typography>
                            </Grid>
                            <Grid item >
                                     <img src={imgSelected} style={{width:"300px", borderRadius:"10px"}}/>
                            </Grid>
                            <Grid container direction="column"
                                justifyContent="flex-start"
                                alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                                {imgUrlEdit === false ?
                                    <Button onClick={() => editImgUrl(true)}>Change Image</Button>
                                    :
                                    <>
                                        <Button color="error" onClick={() => { editImgUrl(false); getImgSelected(productInfo.ImageUrl) }}>Cancel</Button>
                                        <Grid container direction="row"
                                            justifyContent="flex-start"
                                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10, marginTop: 10 }}>
                                            <Grid item>
                                                <Typography>Change Image Product: </Typography>
                                            </Grid>

                                        </Grid>
                                        <Grid container direction="row"
                                            justifyContent="flex-start"
                                            alignItems="center" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10, marginTop: 10 }}>
                                                <Grid container spacing={2} direction="row"
                                                    justifyContent="flex-start"
                                                    alignItems="center">
                                                    <Grid item>
                                                        <img  src={productInfo.ImageUrl} style={{ width: "200px", borderRadius: "40px", padding: 10 }} type="button" onClick={() => { getImgSelected(productInfo.ImageUrl) }}/>
                                                    </Grid>
                                                    {imgList.map((url, index) => {
                                                        return (
                                                            <Grid item>
                                                                <Grid item key={index} className="img-Box">
                                                                    <img className="related-img"
                                                                        src={url} alt="product" style={{ width: "200px", display: "block", marginLeft: "auto", marginRight: "auto", borderRadius: "40px", padding: 10 }}
                                                                        onClick={(url) => getImgSelected(url.target.currentSrc)} />
                                                                    <Grid item className="divColor" style={{ width: 35, height: 35 }} sm={12} xs={12}>
                                                                        <Grid container className="icon-div" alignItems="center" justifyContent="center">
                                                                            <DeleteOutlineIcon type="button" style={{color: "white"}} onClick={()=> deleteImageHandler(url)}/>
                                                                        </Grid>
                                                                    </Grid>
                                                                </Grid>
                                                            </Grid>
                                                        )
                                                    })}
                                                    <Grid item>
                                                        <Grid style={{ width: "180px", height: "180px", backgroundColor: "white", borderRadius: "40px", padding: 10 }}
                                                            justifyContent="center"
                                                            alignItems="center" container direction="row"
                                                        >
                                                            <Button style={{ color: "#d1c286" }} component="label">
                                                                {imgUrl === null || imgUrl === undefined
                                                                    ?
                                                                    <>
                                                                        <AddIcon style={{ fontSize: "50px", padding: 10 }} /><Typography>Add Image </Typography>
                                                                    </>
                                                                    :
                                                                    <Grid justifyContent="flex-start"
                                                                        alignItems="center" container
                                                                        wrap="wrap">
                                                                        <img src={imgUrl} /><Typography> {imgUrl.name.slice(0, 20) + '...'}</Typography>
                                                                    </Grid>
                                                                }
                                                                <input hidden multiple type="file" onChange={(e) => { getImgUrl(e.target.files[0]) }} accept="image/*" />
                                                            </Button>
                                                            <Grid item>
                                                                <Button variant="contained" component="label" onClick={()=>onImageUrlHandler()}>
                                                                    Upload
                                                                </Button>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                        </Grid>
                                    </>
                                }
                            </Grid>
                        </Grid>
                        <Grid container direction="row"
                            justifyContent="flex-end"
                            alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                                <Grid item>
                                    <Button  variant="contained" color="success" onClick={onBtnSaveProduct}>
                                        SAVE PRODUCT
                                    </Button>
                                </Grid>
                            </Grid>
                    </Grid>
                </>
            }
        </>
    )
}

 
export default ProductInfor