import { Col, Container, Row, Breadcrumb, BreadcrumbItem} from "reactstrap";
import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import FilterTable from "../Components/filterComponent/filter";
import { useDispatch, useSelector } from "react-redux";
import { Grid, Pagination, Typography, Box, Tooltip, Menu, MenuItem } from "@mui/material";
import { getLength, getProductInfor, handleCloseFilter } from "../actions/product.action";
import FilterListIcon from '@mui/icons-material/FilterList';
const fetchApi = async(url, body) =>{
    const response = await fetch(url,body);
    const data = await response.json();
    return data;
}
const MoreProduct = () =>{
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [currentPage, getCurrentPage] = useState(1);
    const {condition, status, length, closeFilterTable} = useSelector((reduxData) =>
        reduxData.productReducer
    )
    
    const [Product, showProduct] = useState([]);
   
    const limit = 6;
    const totalProduct = length;
    const noPage = Math.ceil(totalProduct / limit);
    const onChangePagination = (event, value) => {
        getCurrentPage(value);
    }
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
        dispatch(handleCloseFilter(true))
    };
    const handleClose = () => {
        setAnchorEl(null);
        dispatch(handleCloseFilter(false))
    };
    useEffect(()=>{
       if(closeFilterTable === false){
        setAnchorEl(null)
       }
    },[closeFilterTable])
    //Code sử dụng database từ mongoose được tạo từ node.js bài 520.10 
    useEffect(()=>{
        //navigate("/Categories/More");
        if(status === true){
            fetchApi(process.env.REACT_APP_API_SHOP_24H + "/api" + "/products/filter?"+"description="+ condition.description +"&type="+ condition.type +"&minPrice=" + condition.minPrice +"&maxPrice=" + condition.maxPrice)
                .then((data) =>{
                    dispatch(getLength(data.data.length));
                    fetchApi(process.env.REACT_APP_API_SHOP_24H + "/api" + "/products/filter?"+"description="+condition.description+"&type="+condition.type+"&minPrice="+ condition.minPrice + "&maxPrice="+ condition.maxPrice+ "&skipNumber="+((currentPage-1) * limit)+"&limitNumber=" +limit)
                    .then((data)=>{
                        showProduct(data.data);
                    })
                    .catch((error)=>{
                        console.log(error.message)
                    })
                })
                .catch((error)=>{
                    console.log(error.message)
                })
            };
        if(status === false){
            dispatch(getLength());
            fetchApi(process.env.REACT_APP_API_SHOP_24H + "/api" + "/skip-limit-products?skipNumber="+((currentPage-1) * limit)+"&limitNumber=" + limit)
                .then((data) =>{
                    showProduct(data.data);
                })
                .catch((error)=>{
                    console.log(error.message)
                })
        }
    },[currentPage, status, condition, length])

   
    const onProductClick = (value) =>{
        dispatch(getProductInfor(value))
    }
       
    return(
        <Container md={12} sm={12} lg={12} xs={12} className="product-page"> 
            <Breadcrumb>
                <BreadcrumbItem > 
                    <Link to="/" style={{textDecoration:"none"}}>Home</Link>
                </BreadcrumbItem>
                <BreadcrumbItem>
                    <Link to="/Products">Products</Link>
                </BreadcrumbItem>
            </Breadcrumb>
            
            <Col>
                <Col style={{marginTop:"50px", marginBottom:"10px"}}className="d-flex align-items-center justify-content-center">
                    <Row  md={12} sm={12} lg={12} xs={12}>
                        <Col style={{marginRight:"20px"}}>
                            <h4>Products</h4>
                        </Col>
                        <Col>
                            <hr style={{width:"50px",border:"1px solid orange", marginLeft:"-70%"}}/>
                        </Col>
                    </Row>
                </Col>
            </Col>  
                <Row md={12} sm={12} lg={12} xs={12} className="filter-desktop">
                    <Col xs={4} style={{marginTop:"6%", paddingLeft:"15px"}}>
                        <FilterTable/>
                    </Col>
                    <Col xs={8} className="d-flex flex-wrap">
                        {Product? 
                            Product.map((value,index) => {
                                return (
                                    
                                        <Col sm={4} >
                                            
                                            <div className="text-center img-product" key={index}>
                                            {value.Amount ===1? 
                                                <>
                                                    <img src={value.ImageUrl} style={{ width: "90%", height: "90%" }} alt="img"
                                                        onClick={() => { onProductClick(value); navigate("/Products/Details/" + value._id); window.scrollTo({ top: 0, behavior: 'smooth' }) }} />
                                                    <div style={{ marginTop: "15px" }}>
                                                        <h6 style={{ margin: "0px" }}>{value.Name}</h6>
                                                        <p style={{ margin: "0px" }}>Type: <b>{value.Type}</b></p>
                                                        <p style={{ marginTop: "0px" }}><b>${value.PromotionPrice}</b></p>
                                                    </div>
                                                </>
                                                    :
                                                <Grid container className="out-of-stock" direction="row" justifyItems="center" alignItems="center">
                                                    <Grid item>
                                                        <img src={value.ImageUrl} style={{ width: "90%", height: "90%", filter: "brightness(60%)" }} alt="img" />
                                                        <Typography className="content-OOS">
                                                            Out of stock
                                                        </Typography>
                                                    </Grid>
                                                    <Grid container direction="column" justifyItems="center" alignItems="center" style={{ marginTop: "-8px" }}>
                                                        <h6 style={{ margin: "0px" }}>{value.Name}</h6>
                                                        <p style={{ margin: "0px" }}>Type: <b>{value.Type}</b></p>
                                                        <p style={{ marginTop: "0px" }}><b>${value.PromotionPrice}</b></p>
                                                    </Grid>
                                                </Grid>
                                                }
                                                
                                            </div>
                                        </Col>
                                    )
                                }) 
                            : null}
                    </Col>
                </Row>
                <Row md={12} sm={12} lg={12} xs={12} className="filter-mobile">
                    <Row style={{marginTop:"6%", paddingLeft:"15px"}}>
                        <div className=" d-flex flex-row-reverse">
                            <Box sx={{ display: 'flex', alignItems: 'center', textAlign: 'center', justifyContent:"center" }} style={{border:"2px solid #d1c286"}}>
                                <Tooltip>
                                    <FilterListIcon
                                        onClick={handleClick}
                                        size="small"
                                        aria-controls={open ? 'account-menu' : undefined}
                                        aria-haspopup="true"
                                        aria-expanded={open ? 'true' : undefined}
                                        style={{ fontSize: "35px", color: "#d1c286", margin:5 }}
                                    >
                                    </FilterListIcon>
                                </Tooltip>
                            </Box>
                            <Menu
                                anchorEl={anchorEl}
                                id="account-menu"
                                open={open}
                                onClose={handleClose}
                                //onClick={handleClose}
                                PaperProps={{
                                    elevation: 0,
                                    sx: {
                                        overflow: 'visible',
                                        filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                                        mt: 1.5,
                                        '& .MuiAvatar-root': {
                                            width: 32,
                                            height: 32,
                                            ml: -0.5,
                                            mr: 1
                                        },
                                        marginTop: "3px"
                                    },
                                }}
                                transformOrigin={{ horizontal: 'left', vertical: 'top' }}
                                anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                            >
                                <MenuItem style={{fontSize:"12px"}} className = "header-content">
                                    <FilterTable/>
                                </MenuItem>
                            </Menu>
                    </div>
                        
                    </Row>
                    <Col xs={12} className="d-flex flex-wrap">
                        {Product? 
                            Product.map((value,index) => {
                                return (
                                    
                                        <Col sm={4} >
                                            
                                            <div className="text-center img-product" key={index}>
                                            {value.Amount ===1? 
                                                <>
                                                    <img src={value.ImageUrl} style={{ width: "90%", height: "90%" }} alt="img"
                                                        onClick={() => { onProductClick(value); navigate("/Products/Details/" + value._id); window.scrollTo({ top: 0, behavior: 'smooth' }) }} />
                                                    <div style={{ marginTop: "15px" }}>
                                                        <h6 style={{ margin: "0px" }}>{value.Name}</h6>
                                                        <p style={{ margin: "0px" }}>Type: <b>{value.Type}</b></p>
                                                        <p style={{ marginTop: "0px" }}><b>${value.PromotionPrice}</b></p>
                                                    </div>
                                                </>
                                                    :
                                                <Grid container className="out-of-stock" direction="row" justifyItems="center" alignItems="center">
                                                    <Grid item>
                                                        <img src={value.ImageUrl} style={{ width: "90%", height: "90%", filter: "brightness(60%)" }} alt="img" />
                                                        <Typography className="content-OOS">
                                                            Out of stock
                                                        </Typography>
                                                    </Grid>
                                                    <Grid container direction="column" justifyItems="center" alignItems="center" style={{ marginTop: "-8px" }}>
                                                        <h6 style={{ margin: "0px" }}>{value.Name}</h6>
                                                        <p style={{ margin: "0px" }}>Type: <b>{value.Type}</b></p>
                                                        <p style={{ marginTop: "0px" }}><b>${value.PromotionPrice}</b></p>
                                                    </Grid>
                                                </Grid>
                                                }
                                                
                                            </div>
                                        </Col>
                                    )
                                }) 
                            : null}
                    </Col>
                </Row>
                <Grid item md={12} sm={12} lg={12} xs={12} mb={5}
                        container
                        direction="row"
                        justifyContent="center"
                        alignItems="center">
                    <Pagination count={noPage} value={currentPage} onChange={onChangePagination}/>
                </Grid>
        </Container>
    )
}
export default MoreProduct;