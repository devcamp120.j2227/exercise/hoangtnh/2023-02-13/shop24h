import { useState, useEffect } from 'react';
import { Button, Row, Col, Input, FormGroup, Card, CardHeader, CardBody, CardFooter } from 'reactstrap';
import '@fortawesome/fontawesome-free/css/all.min.css';

import auth from '../../firebase';
import { GoogleAuthProvider, signInWithPopup, signOut, onAuthStateChanged } from 'firebase/auth';
import { useDispatch, useSelector } from 'react-redux';
import { loginUserInformation } from '../../actions/product.action';
import { Link, useNavigate } from 'react-router-dom';
import CustomizedSnackbars from '../adminPageComponent/CRUDmodalComponents/toast';
import { loginAccount, openToast } from '../../actions/account.action';

const provider = new GoogleAuthProvider();
function LoginForm() {
//call dispatch để truyền dữ liệu user vào redux
const dispatch = useDispatch();
//khởi tạo các trạng thái ban đầu
const [loginAs, setLoginAs] = useState("Google")

const { loginAsCustomer, accountUser, toast } = useSelector((reduxData) =>
    reduxData.accountReducer
)
const navigate = useNavigate();

const [username, setUsername] = useState("");
const [password, setPassword] = useState("");
const account = {
    username: username,
    password: password
}
//login gg khởi tạo giá trị user ban đầu
const [user, setUser] = useState(null);
const [customer, setCustomer] = useState(null)

//xử lý nút đăng nhập bằng tài khoản gg
const onLogInButton = () => {
    signInWithPopup(auth, provider)
        .then((result) => {
            setUser(result.user)
            setLoginAs("Google");
            navigate("/Cart");
        })
        .catch((error) => {
            console.error(error)
        });
    //tạo tài khoản> đăng nhập > "userAccount" > setItem.localstorage 
}

const onLoginHandler = () => {
    setLoginAs("account");
    if(username ===""){
        dispatch(openToast({open: true,
            message: "User name is required",
            type: "error"}))
    }
    if(password ===""){
        dispatch(openToast({open: true,
            message: "Password is required",
            type: "error"}))
    }
    dispatch(loginAccount(account));
    
}


//giữ lại trạng thái user đăng nhập khi reload trang
useEffect(() => {
    onAuthStateChanged(auth, (result) => {
        setUser(result);
        //truyền user vào redux để thao tác trên check cart
        dispatch(loginUserInformation(result));
    })
}, [user, auth, accountUser]);
useEffect(() => {
    if(loginAsCustomer ==="true password, Login successful"){
        setCustomer(accountUser.data.exitUser);
        setLoginAs("account")
    }
    if(toast.message ==="Login successfull"){
        navigate("/Cart")
    }
}, [ toast, accountUser, customer,loginAs])

  return (
    <>  
    <CustomizedSnackbars/>
        <div className='log-in-page'>
            <Card>
                <CardHeader style={{ textAlign:"center"}}><h4>Sign In For Check Your Cart</h4>
                </CardHeader>
                    <CardBody className="d-flex justify-content-center"  style={{margin:"3%"}}>
                        <Row >
                            <Col className='d-flex justify-content-center' sm={12}>
                                <Button color='danger' style={{ width:"80%",borderRadius:"50px"}}
                                    onClick={onLogInButton}>
                                <i className="fa-brands fa-google"/> Sign In with <b>Google</b> 
                                </Button>
                            </Col>
                            <Col className='d-flex justify-content-center' sm={12}>
                            <div><hr style={{width:"150px",border:"1px solid black"}}/></div>
                            </Col>
                            <Col sm={12} className='d-flex flex-column align-items-center'>
                                <FormGroup style={{width:"80%"}} sm={12}>
                                    <Input
                                    name="username"
                                    placeholder="Username"
                                    style={{borderRadius:"50px"}}
                                    onChange={(e)=> setUsername(e.target.value)}
                                    />
                                </FormGroup>
                                <FormGroup style={{width:"80%"}}>
                                    <Input
                                    name="password"
                                    placeholder="Password"
                                    type="password"
                                    style={{borderRadius:"50px"}}
                                    onChange={(e)=> setPassword(e.target.value)}
                                    />
                                </FormGroup>
                                <Button color='success' style={{ width:"80%",borderRadius:"50px"}}
                                onClick={onLoginHandler}>
                                    Sign In  
                                </Button>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <Col className='text-center'>
                            <p> Don't have an account?  </p>
                            <Link to="/Account"  style={{color:"green"}}>Sign up here</Link>
                        </Col>
                    </CardFooter>
        </Card>
      </div>
    </> 
  );
}

export default LoginForm;