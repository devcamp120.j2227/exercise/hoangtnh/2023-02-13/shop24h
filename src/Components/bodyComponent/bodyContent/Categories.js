import { Button, Col, Row, Breadcrumb, BreadcrumbItem } from "reactstrap";
import { useState } from "react";
import imgCategory from "../../../assets/slideImg/slide3.webp"
import '@fortawesome/fontawesome-free/css/all.min.css';
import { Link, useNavigate } from "react-router-dom";
import { Container } from "@mui/material";


const Categories = () =>{
    const [Product, showProduct] = useState([]);
    const [btnClick, setStatus] = useState(false);
    const navigate = useNavigate();

    return (
        <div className="div-categories">
            <Container md={12} sm={12} lg={12} xs={12}>
                <Row md={12} sm={12} lg={12} xs={12} >
                    <Col style={{marginTop:"50px", paddingLeft:25}} md={5} sm={5} lg={5} xs={5}>
                        <Row className="justify-content-center">
                            <Col xs={3} style={{marginRight:"20px"}}>
                                <h4>Categories</h4>
                            </Col>
                            <Col xs={3}>
                                <hr style={{width:"50px",border:"1px solid orange", marginLeft:"70px"}}/>
                            </Col>
                        </Row>
                        <Row style={{marginTop:"0px"}}>
                            <Row className="d-flex align-items-center mt-5" >
                                <Col xs={1} style={{display:"flex",padding:"0px", justifyContent:"flex-end"}}>
                                    <p type="button"  style={{margin:"0px"}}>01</p>
                                </Col>
                                <Col xs={1}>
                                    <hr style={{width:"30px",border:"1px solid"}}/>
                                </Col>
                                <Col xs={3}>
                                    <p type="button"  style={{margin:" 0 0 0 10px"}}><b>Living Room</b></p>
                                </Col>
                            </Row>
                        </Row>
                        <Row>
                            <Row className="d-flex align-items-center mt-5">
                                <Col xs={1} style={{display:"flex",padding:"0px", justifyContent:"flex-end"}}>
                                    <p type="button"  style={{margin:"0px"}}>02</p>
                                </Col>
                                <Col xs={1}>
                                    <hr style={{width:"30px",border:"1px solid "}}/>
                                </Col>
                                <Col xs={3}>
                                    <p type="button"  style={{margin:" 0 0 0 10px"}}><b>Bed Room</b></p>
                                </Col>
                            </Row>
                        </Row>
                        <Row>
                            <Row className="d-flex align-items-center mt-5">
                                <Col xs={1} style={{display:"flex",padding:"0px", justifyContent:"flex-end"}}>
                                    <p type="button" style={{margin:"0px"}}>03</p>
                                </Col>
                                <Col xs={1}>
                                    <hr style={{width:"30px",border:"1px solid "}}/>
                                </Col>
                                <Col xs={3}>
                                    <p type="button" style={{margin:" 0 0 0 10px"}}><b>Loft Room</b></p>
                                </Col>
                            </Row>
                        </Row>
                        <Row>
                            <Row className="d-flex align-items-center mt-5">
                                <Col xs={1} style={{display:"flex",padding:"0px", justifyContent:"flex-end"}}>
                                    <p type="button"  style={{margin:"0px"}}>04</p>
                                </Col>
                                <Col xs={1}>
                                    <hr style={{width:"30px",border:"1px solid "}}/>
                                </Col>
                                <Col xs={3}>
                                    <p type="button"  style={{margin:" 0 0 0 10px"}}><b>Kitchen</b></p>
                                </Col>
                            </Row>
                        </Row>
                        
                    </Col>
                    <Col style={{marginTop:"100px", display:"flex", alignItems:"center"}} md={7} sm={7} lg={7} xs={7}>
                        <Row style={{display: "flex" ,flexDirection: "row"}}>
                            <img src={imgCategory} style={{borderRadius:"5px", padding:"0px"}}/>
                        </Row>
                    </Col>
                    <Row>
                        <Button style={{ width: "200px", color: "#d1c286", marginTop: "20px" }} onClick={() => { setStatus(false); navigate("/Products") }}> For More Product <i className="fa-solid fa-angles-right"></i></Button>
                    </Row>
                </Row>
            </Container>
        </div>
    )
}
export default Categories;