import { Col, Row } from "reactstrap";
import '@fortawesome/fontawesome-free/css/all.min.css';
import { useNavigate } from "react-router-dom";
import LoginModal from "../modalComponent/loginModal";
import { useDispatch, useSelector } from "react-redux";
import { useState } from "react";
import ShoppingCart from "../modalComponent/checkCartBag";
import { changeCartStatus } from "../../actions/product.action";
import { Box, Grid, Menu, MenuItem, Tooltip, Typography } from "@mui/material";
import MenuIcon from '@mui/icons-material/Menu';
const Header = () =>{
    const navigate = useNavigate();
    const onLogoClick = () =>{
        navigate("/");
    }
    const {numberProduct, checkCartStatus} = useSelector((reduxData)=>
    reduxData.productReducer
    )
    const onBtnProductClick = () =>{
        navigate("/Products");
    }
   
    const dispatch = useDispatch();
    const [status, setStatus] = useState(false)
    const onCheckCartHandler = () =>{
        if(numberProduct > 0){
            setStatus(!status)
            dispatch(changeCartStatus(!checkCartStatus))
        }
    }
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    return (
        <>
        {checkCartStatus? <ShoppingCart/>:null}
            <div className="desktop-display" style={{backgroundColor:"#d1c286", width:"100%"}}>
                    <Row md={12} sm={12} lg={12} xs={12} style={{padding:"10px"}} >
                        <Col md={3} sm={3} lg={3} xs={3} className="d-flex align-items-center justify-content-center" >
                            <h3 style={{color:"white"}} onClick={onLogoClick} type="button">Devcamp COSY-HOME</h3>
                        </Col>
                        <Col md={7} sm={7} lg={7} xs={7}  className="d-flex align-items-center justify-content-center" style={{padding:"0px"}}>
                            <Row xs={12} style={{margin:"0px", width:"100%"}}>
                                <Col sm={3} className = "header-content text-center" style={{padding:"5px"}}>
                                    <a href="#" onClick={onBtnProductClick}>PRODUCT</a>
                                </Col>
                                <Col sm={3} className = "header-content text-center" style={{padding:"5px"}}>
                                    <a href="#">ABOUT</a>
                                </Col>
                                <Col sm={3} className = "header-content text-center" style={{padding:"5px"}}>
                                    <a href="#" >SERVICES</a>
                                </Col>
                                <Col sm={3} className = "header-content text-center"style={{padding:"5px"}}>
                                    <a href="#" >CONTACT</a>
                                </Col>
                            </Row>
                        </Col>
                        <Col  md={2} sm={2} lg={2} xs={2}  className="d-flex align-items-center justify-content-end">
                            <Col md={8} sm={8} lg={8} xs={8} className="awesome-icon d-flex align-items-center justify-content-end" style={{marginRight:"5px"}}>
                                <p style={{margin:"0px"}} onClick={()=>onCheckCartHandler()} type="button">
                                    <i type="button" className="fa-solid fa-cart-shopping " style={{fontSize:"18px"}}></i>
                                        {numberProduct !== 0?<span className='badge badge-warning' id='lblCartCountHeader'> {numberProduct} </span>:null}
                                    Cart</p>
                            </Col>
                            <Col md={4} sm={4} lg={4} xs={4}  className="awesome-icon d-flex align-items-center justify-content-center">
                                <LoginModal/>
                            </Col>
                        </Col>
                    </Row> 
            </div>
            <div className="mobile-display" style={{backgroundColor:"#d1c286", width:"100%"}}>
                <Grid container direction="row"
                    justifyContent="center"
                    alignItems="center" xs={12}>
                    <Grid item xs={3}>
                            <Box sx={{ display: 'flex', alignItems: 'center', textAlign: 'center' }}>
                                <Tooltip>
                                    <MenuIcon
                                        onClick={handleClick}
                                        size="small"
                                        sx={{ ml: 2 }}
                                        aria-controls={open ? 'account-menu' : undefined}
                                        aria-haspopup="true"
                                        aria-expanded={open ? 'true' : undefined}
                                        style={{ fontSize: "30px", color: "white" }}
                                    >
                                    </MenuIcon>
                                </Tooltip>
                            </Box>
                            <Menu
                                anchorEl={anchorEl}
                                id="account-menu"
                                open={open}
                                onClose={handleClose}
                                onClick={handleClose}
                                PaperProps={{
                                    elevation: 0,
                                    sx: {
                                        overflow: 'visible',
                                        filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                                        mt: 1.5,
                                        '& .MuiAvatar-root': {
                                            width: 32,
                                            height: 32,
                                            ml: -0.5,
                                            mr: 1
                                        },
                                        marginTop: "3px",
                                        width: "120px"
                                    },
                                }}
                                transformOrigin={{ horizontal: 'left', vertical: 'top' }}
                                anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                            >
                                <MenuItem onClick={handleClose} style={{fontSize:"12px"}} className = "header-content">
                                    <a  href="#" onClick={onBtnProductClick} >PRODUCT</a>
                                </MenuItem>
                                <MenuItem onClick={handleClose} style={{fontSize:"12px"}} className = "header-content">
                                    <a href="#">ABOUT</a>
                                </MenuItem>
                                <MenuItem onClick={handleClose} style={{fontSize:"12px"}} className = "header-content">
                                    <a href="#">SERVICES</a>
                                </MenuItem>
                                <MenuItem onClick={handleClose} style={{fontSize:"12px"}} className = "header-content">
                                    <a href="#">CONTACT</a>
                                </MenuItem>
                            </Menu>
                    </Grid>
                    <Grid item xs={6} >
                        <Grid container direction="row" justifyContent="center" textAlign="center">
                            <Typography style={{ fontSize: "18px", color:"white" }} onClick={onLogoClick}>
                                Devcamp COSY-HOME
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid item xs={3}>
                        <Grid container direction="row" justifyContent="center" alignItems="center">
                            <Grid item mr={1}>
                                    <i type="button" className="fa-solid fa-cart-shopping " onClick={() => onCheckCartHandler()} style={{ fontSize: "14px"}}></i>
                                    {numberProduct !== 0 ? <span className='badge badge-warning' id='lblCartCountHeader' style={{marginRight:"5px"}}> {numberProduct} </span> : null}
                            </Grid>
                            <Grid item style={{width:"14px"}}>
                                <LoginModal/>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        </>
    )
}
export default Header;