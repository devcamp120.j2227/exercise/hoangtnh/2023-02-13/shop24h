import { Link } from "react-router-dom";
import { Breadcrumb, BreadcrumbItem } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import CustomizedSnackbars from "../CRUDmodalComponents/toast";
import { Grid, Typography, TextField, TextareaAutosize, Button, InputLabel, FormControl, Select, MenuItem } from "@mui/material";
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import { useState } from "react";
import { useEffect } from "react";
import { getAddressDetail, getAddressUser, getDistrict, getProvide, getWard } from "../../../actions/product.action";
import TableAddProduct from "./tableAddProduct";
import CartBagTable from "./tableCartBag";
import { createNewOrder } from "../../../actions/account.action";

const fetchApi = async(url, body) =>{
    const response = await fetch(url,body);
    const data = await response.json();
    
    return data;
}
const CreateNewOrder = () =>{
    const {toast, cartBag} = useSelector((reduxData)=>
        reduxData.accountReducer
    );
    const {ward, address, district, selectedDistrict, selectedProvide, selectedWard, selectedAddress} = useSelector((reduxData)=>
        reduxData.productReducer
    );
    const dispatch = useDispatch();
    useEffect(()=>{
        
        fetchApi("https://provinces.open-api.vn/api/?depth=3")
            .then((data)=>{
                dispatch(getAddressUser(data))
            })
            .catch((error)=>{
                console.log(error.message)
            });
    },[])
    const [customerName, getCustomerName] = useState("");
    const [phoneNumber, getPhoneNumber] = useState("");
    const [email, getEmail] = useState ("");

    const handleProvideChange = (e) =>{
        dispatch(getProvide(e.target.value));
    }
    const handleDistrictChange = (e) =>{
        dispatch(getDistrict(e.target.value));
    }
    const handleWardChange = (e)=>{
        dispatch(getWard(e.target.value))
    }
    const onAddressHandler = (e) =>{
        dispatch(getAddressDetail(e.target.value))
    }
   
    const onBtnCreateOrder = ()=>{
        const newOrder = cartBag;
        const customer ={
        Name: customerName,
        Email: email,
        Phone: phoneNumber,
        Address: {
            Provide: selectedProvide,
            District: selectedDistrict,
            Ward: selectedWard,
            Address: selectedAddress
            }
        }
        dispatch(createNewOrder(customer, newOrder))
    }
    return (
        <>
        <CustomizedSnackbars/>
            <Breadcrumb>
                <BreadcrumbItem >
                    <Link to="/Administrator/Orders" style={{ textDecoration: "none" }}>Orders</Link>
                </BreadcrumbItem>
                <BreadcrumbItem >
                    <Link>Create order</Link>
                </BreadcrumbItem>
            </Breadcrumb>

            <Grid container direction="column"
                justifyContent="flex-start"
                alignItems="flex-start"
                style={{ backgroundColor: "whiteSmoke", padding: 20, borderRadius: "20px" }}
                md={12} sm={12} lg={12} xs={12} spacing={1} >
                <Typography style={{ color: "#d1c286" }}>
                    <b><ArrowRightIcon />NEW ORDER</b>
                </Typography>
                <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="center" spacing={2} xs={12} style={{ padding: 10 }}>
                    <Grid item  sm={2}>
                        <Typography>
                            <b>Customer Name:</b>
                        </Typography>
                    </Grid>
                    <Grid item >
                        <TextField variant="outlined" label="Customer name" defaultValue={customerName} onChange={(e) => getCustomerName(e.target.value)} />
                    </Grid>
                </Grid>
                <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="center" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                    <Grid item sm={2}>
                        <Typography>
                            <b>Phone Number:</b>
                        </Typography>
                    </Grid>
                    <Grid item >
                        <TextField type="number" variant="outlined" label="Phone number" defaultValue={phoneNumber} onChange={(e) => getPhoneNumber(e.target.value)} />
                    </Grid>
                </Grid>
                <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="center" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                    <Grid item sm={2}>
                        <Typography >
                            <b>Email:</b>
                        </Typography>
                    </Grid>
                    <Grid item >
                        <TextField variant="outlined" label="Email" defaultValue={email} onChange={(e) => getEmail(e.target.value)} />
                    </Grid>
                </Grid>
                <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                    <Grid item sm={2}>
                        <Typography>
                            <b>Address:</b>
                        </Typography>
                    </Grid>
                    <Grid item sm={10}>
                        <Grid container spacing={2} direction="column" md={12} sm={12} lg={12} xs={12}>
                            <Grid item md={12} sm={12} lg={12} xs={12}>
                                <Grid container direction="row" alignItems="center" spacing={1} sm={12}>
                                    <Grid item sm ={6}>
                                        <Grid item sm={3} mb={1}>
                                            Province:
                                        </Grid>
                                        <Grid item sm={9}>
                                            <FormControl fullWidth>
                                                <InputLabel>Choose province</InputLabel>
                                                <Select
                                                    label="Choose province"
                                                    value={selectedProvide}
                                                    onChange={handleProvideChange}
                                                    sx={{ width: 250 }}
                                                >
                                                    {address.map((value, index) => {
                                                        return (
                                                            <MenuItem value={value.name} key={index}>{value.name}</MenuItem>
                                                        )
                                                    })}
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                    <Grid item sm ={6}>
                                        <Grid item sm={3} mb={1}>
                                            District:
                                        </Grid>
                                        <Grid item sm={9}>
                                            <FormControl fullWidth>
                                                <InputLabel>Choose district</InputLabel>
                                                <Select
                                                    value={selectedDistrict}
                                                    label="Choose province"
                                                    onChange={handleDistrictChange}
                                                    sx={{ width: 250 }}
                                                >
                                                    {district.map((value, index) => {
                                                        return (
                                                            <MenuItem value={value.name} key={index}>{value.name}</MenuItem>
                                                        )
                                                    })}
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item  md={12} sm={12} lg={12} xs={12}>
                                <Grid container direction="row" alignItems="center" spacing={1} sm={12}>
                                    <Grid item sm={6}>
                                        <Grid item sm={3} mb={1}>
                                            Ward:
                                        </Grid>
                                        <Grid item sm={9}>
                                            <FormControl >
                                                <InputLabel>Choose ward</InputLabel>
                                                <Select
                                                    value={selectedWard}
                                                    label="Choose ward"
                                                    onChange={handleWardChange}
                                                    sx={{ width: 250 }}
                                                >
                                                    {ward.map((value, index) => {
                                                        return (
                                                            <MenuItem value={value.name} key={index}>{value.name}</MenuItem>
                                                        )
                                                    })}
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                    <Grid item sm={6}>
                                        <Grid item sm={3}mb={1}>
                                            Address:
                                        </Grid>
                                        <Grid item sm={9}>
                                            <FormControl >
                                                <TextField value={selectedAddress} sx={{ width: 250 }} label="Address" variant="outlined" onChange={onAddressHandler} />
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                    <Grid item sm={2} mt={3}>
                        <Typography>
                            <b>Product:</b>
                        </Typography>
                    </Grid>
                    <Grid item sm={10}>
                        <TableAddProduct/>
                    </Grid>
                </Grid>
                
                <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                    <Grid item sm={2}  mt={3}>
                        <Typography>
                            <b>Cart Bag:</b>
                        </Typography>
                    </Grid>
                    <Grid item sm={6}>
                        <CartBagTable/>
                    </Grid>
                </Grid>
                <Grid container direction="row"
                    justifyContent="flex-end"
                    alignItems="flex-start" spacing={2} md={12} sm={12} lg={12} xs={12} style={{ padding: 10 }}>
                    <Grid item>
                        <Button variant="contained" style={{backgroundColor:"#d1c286"}} onClick={onBtnCreateOrder}>
                            CREATE ORDER
                        </Button>
                    </Grid>
                </Grid> 
            </Grid>
        </>
    )
}
export default CreateNewOrder