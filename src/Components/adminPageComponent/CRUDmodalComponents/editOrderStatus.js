import { useDispatch, useSelector } from "react-redux"
import { Dialog, Box, Typography, Grid, FormControl, InputLabel, Select, MenuItem, TextField, DialogTitle, DialogContent, DialogActions, Button } from "@mui/material";
import { setStatusEdit, handleChangeStatusOrder, fetchApiChangeStatusOrder, filterOrderByStatus } from "../../../actions/account.action";
import CustomizedSnackbars from "./toast";

const EditStatus = () =>{
    const dispatch = useDispatch();
    const {orderInfor, statusEdit, statusOrder, toast, orderStatus} = useSelector((reduxData)=>
        reduxData.accountReducer
    )
    const handleChange =(e) =>{
        dispatch(handleChangeStatusOrder(e.target.value));
    }
    const onBtnEditHandler = ()=>{
        dispatch(fetchApiChangeStatusOrder(statusOrder, orderInfor.orderId));
        if(orderStatus !=="All Status"){
            dispatch(filterOrderByStatus(orderStatus))
        }
        //dispatch(callApiGetAllOrder());
   }
   const handleClose = () =>{
       dispatch(setStatusEdit(false));
   }
//    const orderId = orderInfor.orderId.substr(orderInfor.orderId.length - 6);
//    console.log(orderId)
   const status = [ 
    {
      value: 'Ordered',
      label: 'Ordered',
    },
    {
      value: 'On Process',
      label: 'On Process',
    },
    {
      value: 'Finished',
      label: 'Finished',
    },
    {
      value: 'Canceled',
      label: 'Canceled',
    },
  ];
    return (
        <>
        {Object.keys(orderInfor).length === 0 ? null :
            <Dialog open= {statusEdit} scroll= "paper" component="form" onClose={handleClose} onSubmit={onBtnEditHandler}> {/* */} 
                    <DialogTitle>
                        <Typography textAlign="center" variant="h5" >
                            Edit Status
                        </Typography>
                    </DialogTitle>
                    <DialogContent dividers='paper'>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2} direction="row"
                            justifyContent="flex-start"
                            alignItems="center">
                            <Grid item xs={4}>
                                <Typography>Order Code:</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {orderInfor.orderId}
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true, style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                            </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2} direction="row"
                            justifyContent="flex-start"
                            alignItems="center">
                            <Grid item xs={4}>
                                <Typography>Total:</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {"$"+orderInfor.total}
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true, style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                            </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2} direction="row"
                            justifyContent="flex-start"
                            alignItems="center">
                            <Grid item xs={4}>
                                <Typography>Customer:</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {orderInfor.customer === null ? "none": orderInfor.customer.Name}
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true ,style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2} direction="row"
                            justifyContent="flex-start"
                            alignItems="center">
                            <Grid item xs={4}>
                                <Typography>Address:</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {orderInfor.customer ===null ? "none":(orderInfor.customer.Address.Provide)+", "+(orderInfor.customer.Address.District)+", "+(orderInfor.customer.Address.Ward)+", "+(orderInfor.customer.Address.Address)} 
                                    variant="filled"
                                    fullWidth
                                    multiline
                                    InputProps={{ 
                                        disableUnderline: true ,style:{paddingBottom:"10px", height:"100px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2} direction="row"
                            justifyContent="flex-start"
                            alignItems="center">
                            <Grid item xs={4} >
                                <Typography>Phone number:</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {orderInfor.customer ===null ? "none":orderInfor.customer.Phone}
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true ,style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:1}}>
                        <Grid container spacing={2} direction="row"
                            justifyContent="flex-start"
                            alignItems="center">
                            <Grid item xs={4}>
                                <Typography>Email:</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    disabled
                                    value= {orderInfor.customer ===null ? "none":orderInfor.customer.Email}
                                    variant="filled"
                                    fullWidth
                                    InputProps={{ 
                                        disableUnderline: true ,style:{paddingBottom:"10px", height:"50px", borderRadius:"5px"}
                                    }}
                                    />
                                </Grid>
                        </Grid>
                    </Typography>
                    <Typography sx ={{mt:2}}>
                        <Grid container spacing={2} direction="row"
                            justifyContent="flex-start"
                            alignItems="center">
                            <Grid item xs={4}>
                                <Typography>Status:</Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <Box sx={{ minWidth: 120 }}>
                                    <FormControl fullWidth>
                                        <InputLabel>Change Status</InputLabel>
                                        <Select value={statusOrder !==""? statusOrder: orderInfor.status} name="trangThai" label="Change Status" onChange={handleChange}
                                            >
                                            {status.map((option) => (
                                                <MenuItem key={option.value} value={option.value}>
                                                {option.label}
                                                </MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl>
                                </Box>
                            </Grid>
                        </Grid>
                    </Typography>
                    </DialogContent>
                    <DialogActions>
                        <Button  onClick={handleClose} variant="outlined"  color="error">Close</Button>
                        <Button  variant="contained" onClick={onBtnEditHandler}>Edit Status</Button>
                    </DialogActions>
            </Dialog>
            }
        {toast.open? <CustomizedSnackbars/>: null}

        </>
    )
}
export default EditStatus;