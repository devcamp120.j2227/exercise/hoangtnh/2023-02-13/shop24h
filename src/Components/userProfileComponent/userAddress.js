import { Grid, TextField, Select, MenuItem, InputLabel, FormControl, Button, ThemeProvider,Modal, Box, Typography, Autocomplete } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAddressDetail, getDistrict, getProvide, getWard, setPage } from "../../actions/product.action";
import { theme } from "./userProfile";
import {style} from "./userPayment";
import avatarDefault from "../../assets/default avatar/png-transparent-default-avatar-thumbnail.png"

const fetchApi = async(url, body) =>{
    const response = await fetch(url,body);
    const data = await response.json();
    
    return data;
}

const Address = () =>{
    const dispatch = useDispatch();
    const {ward, address, district, selectedDistrict, selectedProvide, selectedWard, selectedAddress, user} = useSelector((reduxData)=>
        reduxData.productReducer
    );
    const {accountUser} =  useSelector((reduxData)=>
        reduxData.accountReducer
    );
    const [open, setOpen] = useState(false);
    const handleClose = () => setOpen(false);
    const [uid, setUid] = useState("");
    const [email, setEmail] = useState("");
    
    useEffect(()=>{
        if(accountUser.data !== undefined){
            setUid(accountUser.data.exitUser._id)
            setEmail(accountUser.data.exitUser.Email);
        }
        if(user !== null){
            setUid(user.uid)
            setEmail(user.email)
        }
    }, [accountUser, user, uid])
    const onBtnSaveHandler = () =>{
        var requestOptions = {
            method: 'PUT',
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                Address: {Provide: `${selectedProvide}`, District: `${selectedDistrict}`,Ward: `${selectedWard}`,Address:  `${selectedAddress}`},
            }),
            redirect: 'follow'
          };
          if(selectedProvide !== "" && selectedDistrict !=="" && selectedWard !== "" && selectedAddress !== ""){
            fetchApi(process.env.REACT_APP_API_SHOP_24H + "/api" + "/customers?userId="+ uid, requestOptions)
                .then((data)=>{                    
                    if(data.data === null){
                        const requestOptions = {
                            method: 'POST',
                            headers: {
                                'Content-type': 'application/json; charset=UTF-8'
                            },
                            body: JSON.stringify({
                                UserId: uid,
                                Email: email,
                                Address: {Provide: selectedProvide, District: selectedDistrict,Ward: selectedWard,Address:  selectedAddress},
                                }),
                            redirect: 'follow'
                            };
                        fetchApi(process.env.REACT_APP_API_SHOP_24H + "/api" + "/customers", requestOptions)
                                .then((data)=>{
                                    dispatch(setPage("payment"))
                                })
                                .catch((error)=>{
                                    console.log(error.message);
                                })
                    }
                    else{
                        dispatch(getProvide(data.data.Address.Provide)); 
                        dispatch(getDistrict(data.data.Address.District)); 
                        dispatch(getWard(data.data.Address.Ward)); 
                        dispatch(getAddressDetail(data.data.Address.Address));
                        dispatch(setPage("payment"))
                    }

                })
            .catch((error)=>{
                console.log(error.message);
            })
        }
        else{
            setOpen(true);
        }
          
    }
    const handleProvideChange = (e) =>{
        dispatch(getProvide(e));
    }
    const handleDistrictChange = (e) =>{
        dispatch(getDistrict(e));
    }
    const handleWardChange = (e)=>{
        dispatch(getWard(e))
    }
    const onAddressHandler = (e) =>{
        dispatch(getAddressDetail(e.target.value))
    }
    useEffect(()=>{
        const userInfor = JSON.parse(localStorage.getItem("userAccount"))
            var requestOptions = {
                method: 'GET',
                headers: {"Authorization": `Bearer ${userInfor.data.accessToken}`},
                redirect: 'follow'
              }
        fetchApi(process.env.REACT_APP_API_SHOP_24H + "/api" + "/customers?userId="+ uid, requestOptions)
            .then((data)=>{
                dispatch(getProvide(data.data.Address.Provide));
                dispatch(getDistrict(data.data.Address.District));
                dispatch(getWard(data.data.Address.Ward));
                dispatch(getAddressDetail(data.data.Address.Address));
            })
            .catch((error)=>{
                console.log(error.message)
            });
    },[user, accountUser, uid])
    useEffect(()=>{

    },[selectedAddress, selectedDistrict, selectedProvide, selectedWard])
    return(
            <>
                    
                <Grid container direction="column" className="user-address-desktop"
                        justifyContent="flex-start" alignItems="flex-start"
                        xs={9} style={{backgroundColor:"whiteSmoke", padding: 10}}>
                    <Grid item> 
                        <h3><b>My Address</b></h3>
                    </Grid>
                    <Grid item style={{fontSize: 12, fontStyle:"italic"}}>
                        Choose your address to receive goods
                    </Grid>
                    <br/>
                    <Grid container spacing={2} direction="column"  md={12} sm={12} lg={12} xs={12}>
                        <Grid item>
                            <Grid container direction="row" alignItems="center" spacing={1}>
                                    <Grid item xs={2}>
                                        Province: 
                                    </Grid>
                                    <Grid item>
                                        <FormControl fullWidth>
                                            <Autocomplete
                                                disablePortal
                                                options={address.map((value)=> {return value.name})}
                                                sx={{ width: 250 }}
                                                renderInput={(params) => <TextField {...params} label="Provide"/>}
                                                onChange={(e, newValue) =>{handleProvideChange(newValue)}}
                                                value={selectedProvide}
                                                
                                            />
                    
                                        </FormControl>
                                    </Grid>
                                </Grid>
                            </Grid>
                        <Grid item >
                            <Grid container direction="row" alignItems="center" spacing={1}>
                                <Grid item xs={2}>
                                    District: 
                                </Grid>
                                <Grid item>
                                    <FormControl fullWidth>
                                        <Autocomplete
                                                disablePortal
                                                options={district.map((value)=> {return value.name})}
                                                sx={{ width: 250 }}
                                                renderInput={(params) => <TextField {...params} label="District"/>}
                                                onChange={(e, newValue) =>{handleDistrictChange(newValue)}}
                                                value={selectedDistrict}
                                            />
                                        
                                    </FormControl>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Grid container direction="row" alignItems="center" spacing={1} >
                                <Grid item xs={2}>
                                    Ward: 
                                </Grid>
                                <Grid item>
                                    <FormControl fullWidth>
                                        <Autocomplete
                                            disablePortal
                                            options={ward.map((value) => { return value.name })}
                                            sx={{ width: 250 }}
                                            renderInput={(params) => <TextField {...params} label="Ward" />}
                                            onChange={(e, newValue) => { handleWardChange(newValue) }}
                                            value={selectedWard}
                                        />
                                        
                                    </FormControl>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Grid container alignItems="center">
                                <Grid item xs={2}>
                                    Address: &nbsp;
                                </Grid>
                                <Grid item >
                                    <TextField value={selectedAddress} sx={{width:250}} label="Address" variant="outlined" onChange={onAddressHandler}/>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Grid container>
                                <Button variant="contained" style={{backgroundColor: "#d1c286"}} onClick={onBtnSaveHandler}>
                                    Save & Check out
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                    
                <Grid container direction="row" className="user-address-mobile"
                        justifyContent="flex-start" alignItems="flex-start" 
                        style={{backgroundColor:"whiteSmoke", padding: 10, width:"100%"}}>
                    <Grid item> 
                        <h3><b>My Address</b></h3>
                    </Grid>
                    <Grid item style={{fontSize: 12, fontStyle:"italic"}}>
                        Choose your address to receive goods
                    </Grid>
                    <br/>
                    <Grid container direction="column" mt={2}>
                        <Grid item>
                            <Grid container direction="row" alignItems="center" spacing={1}>
                                    <Grid item xs={3}>
                                        Province: 
                                    </Grid>
                                    <Grid item>
                                        <FormControl fullWidth>
                                            <Autocomplete
                                                disablePortal
                                                options={address.map((value)=> {return value.name})}
                                                sx={{ width: 250 }}
                                                renderInput={(params) => <TextField {...params} label="Provide"/>}
                                                onChange={(e, newValue) =>{handleProvideChange(newValue)}}
                                                value={selectedProvide}
                                                
                                            />
                                            
                                        </FormControl>
                                    </Grid>
                                </Grid>
                            </Grid>
                        <Grid item >
                            <Grid container direction="row" alignItems="center" spacing={1}>
                                <Grid item xs={3}>
                                    District: 
                                </Grid>
                                <Grid item>
                                    <FormControl fullWidth>
                                        <Autocomplete
                                                disablePortal
                                                options={district.map((value)=> {return value.name})}
                                                sx={{ width: 250 }}
                                                renderInput={(params) => <TextField {...params} label="District"/>}
                                                onChange={(e, newValue) =>{handleDistrictChange(newValue)}}
                                                value={selectedDistrict}
                                            />
                                       
                                    </FormControl>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Grid container direction="row" alignItems="center" spacing={1} >
                                <Grid item xs={3}>
                                    Ward: 
                                </Grid>
                                <Grid item>
                                    <FormControl fullWidth>
                                        <Autocomplete
                                            disablePortal
                                            options={ward.map((value) => { return value.name })}
                                            sx={{ width: 250 }}
                                            renderInput={(params) => <TextField {...params} label="Ward" />}
                                            onChange={(e, newValue) => { handleWardChange(newValue) }}
                                            value={selectedWard}
                                        />
                                        
                                    </FormControl>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Grid container alignItems="center">
                                <Grid item xs={3}>
                                    Address: &nbsp;
                                </Grid>
                                <Grid item >
                                    <TextField value={selectedAddress} sx={{width:250}} label="Address" variant="outlined" onChange={onAddressHandler}/>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Grid container>
                                <Button variant="contained" style={{backgroundColor: "#d1c286"}} onClick={onBtnSaveHandler}>
                                    Save & Check out
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                
                <Modal
                    open={open}
                    onClose={handleClose}
                    >
                    <Box sx={style}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                        <b>Kindly Give Your Address</b>
                        </Typography>
                    </Box>
                </Modal>
            </>
    )
}
export default Address;
