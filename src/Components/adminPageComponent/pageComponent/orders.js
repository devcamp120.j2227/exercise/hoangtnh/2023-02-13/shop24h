import * as React from "react";
import IconButton from "@mui/material/IconButton";
import Collapse from "@mui/material/Collapse";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import { useDispatch, useSelector } from "react-redux";
import { getAllOrders, getLength, getShortOrderId, getCurrentPage, filterOrderByStatus, setStatusEdit, getOrderInfo, deleteOrderHandler, setStatusDelete, filterOrderByPhoneNumber, getDateResult } from "../../../actions/account.action";
import { Button,TextField,MenuItem, Grid, Typography, TableContainer, Table, TableRow, TableCell, TableBody, Paper, TableHead, Box, Pagination, ClickAwayListener, Tooltip } from "@mui/material";
import  EditStatus  from "../CRUDmodalComponents/editOrderStatus";
import DeleteIcon from '@mui/icons-material/Delete';
import { DeleteModal } from "../CRUDmodalComponents/deleteModal";
import { Link } from "react-router-dom";
import AddIcon from '@mui/icons-material/Add';
import { useState } from "react";
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file
import { DateRange } from 'react-date-range';
import { useEffect } from "react";
import useDebounce from "../../../hooks/useDelay";
const fetchApi = async(url, body) =>{
    const response = await fetch(url,body);
    const data = await response.json();
    return data;
}

const OrdersInfo = () =>{
    const dispatch = useDispatch();
    const {orders, length, currentPage, limit, orderStatus, account, toast, phoneSearch, date, noPage} = useSelector((reduxData)=>
            reduxData.accountReducer
    )
    const [phoneNumber , setPhoneNumber ]= useState("");
    //thay đổi số trang
    const onChangePagination = (event, value) => {
        dispatch(getCurrentPage(value));
    }
    
    //filter order by status
    const onStatusOrderFilter = (e) => {
        dispatch(filterOrderByStatus(e.target.value))
    }
    //filter order by phone number
    const phoneNumberDelay =  useDebounce(phoneNumber, 500)
    useEffect(()=>{
        dispatch(filterOrderByPhoneNumber(phoneNumberDelay))
    }, [phoneNumberDelay])

    const [dateRange, setDateRange] = useState(date);

    const [open, setOpen] = useState(false);

    //filter order by id
    const [shortId, setShortId]= useState("");
    const shortIdDelay = useDebounce(shortId, 500);
    useEffect(()=>{
        dispatch(getShortOrderId(shortIdDelay, currentPage))
    },[shortIdDelay])
    
    const handleTooltipClose = () => {
        setOpen(false);
    };

    const handleTooltipOpen = () => {
        setOpen(true);
    };
    //filter order by date
    const onSearchByDate = () => {
        dispatch(getDateResult(dateRange, currentPage, limit))
    }
    useEffect(()=>{
        (async () => {
        if(orderStatus ==="None" && phoneSearch ==="" && shortId ===""){
            dispatch(getDateResult(dateRange, currentPage, limit))
        }
        })()
    },[currentPage, length])
    React.useEffect(()=>{
        (async () => {
            if(orderStatus === "All Status"){
                var requestOptions = {
                    method: 'GET',
                    headers:{"Authorization": `Bearer ${account.accessToken}`},
                    redirect: 'follow'
                };
                //lấy length order
                await fetchApi(process.env.REACT_APP_API_SHOP_24H + "/api" + "/orders", requestOptions)
                    .then((data)=>{
                        dispatch(getLength(data.data))
                    })
                    .catch((error) => {
                        console.log( error.message)
                    });
                //lấy skip limit order để phân trang
                await fetchApi(process.env.REACT_APP_API_SHOP_24H + "/api" + "/orders?skipNumber="+((currentPage-1) * limit)+"&limitNumber=" + limit, requestOptions)
                    .then((data)=>{
                        dispatch(getAllOrders(data.data))
                    })
                    .catch((error) => {
                        console.log(error.message)
                    });
                }
        })
        ()
        
    },[currentPage, orderStatus, toast ]);

    const status = [
        {
            value: 'All Status',
            label: 'All Status',
        },
        {
            value: 'Ordered',
            label: 'Ordered',
        },
        {
            value: 'On Process',
            label: 'On Process',
        },
        {
            value: 'Finished',
            label: 'Finished',
        },
        {
            value: 'Canceled',
            label: 'Canceled',
        },
      ];
     
      
    return (
        <>
            <Grid container direction="row"
                alignItems="center" spacing={2} style={{ marginBottom: "10px", marginTop:"20px" }}>
                <Grid item xs={3}>
                    <Grid container direction="row"
                        justifyContent="flex-start"
                        alignItems="center">
                        <Grid item>
                            <Link style={{textDecoration:"none"}} to="/Administrator/CreateOrder">
                                <Button style={{backgroundColor:"#d1c286", color:"white", fontSize:"16px"}}><AddIcon/>Create New Order</Button>
                            </Link>
                        </Grid>
                    </Grid>
                    
                </Grid>
                <Grid item xs={9}>
                    <Grid container direction="row"
                        justifyContent="flex-end"
                        alignItems="flex-end" spacing={2} style={{ marginBottom: "10px" }}>
                        <Grid item >
                            <ClickAwayListener onClickAway={handleTooltipClose}>
                                <div>
                                <Tooltip
                                    placement="top-start"
                                    PopperProps={{
                                    disablePortal: true,
                                    }}
                                    onClose={handleTooltipClose}
                                    open={open}
                                    disableFocusListener
                                    disableHoverListener
                                    disableTouchListener
                                    title={<Grid container justifyContent="flex-end"><DateRange
                                        onChange={(item) => {setDateRange([item.selection])}}
                                        showSelectionPreview={false}
                                        moveRangeOnFirstSelection={false}
                                        months={2}
                                        ranges={dateRange}
                                        direction="vertical"
                                        style={{fontFamily: "Comfortaa"}}
                                    /><Button style={{backgroundColor:"#d1c286", color:"white"}} onClick={onSearchByDate}>Search</Button></Grid>}
                                    
                                >
                                    <Button onClick={handleTooltipOpen} variant="standard">Filter Date</Button>
                                </Tooltip>
                                </div>
                            </ClickAwayListener>
                        </Grid>
                        <Grid item>
                            <TextField label="Order Id" value={shortId} variant="standard" onChange={(e) => setShortId(e.target.value)} />
                        </Grid>
                        
                        <Grid item>
                            <TextField
                                select
                                label="Find status order"
                                value={orderStatus}
                                variant="standard"
                                style={{ width: "150px" }}
                                onChange={(e) => onStatusOrderFilter(e)}
                            >
                                {status.map((option) => (
                                    <MenuItem key={option.value} value={option.value}>
                                        {option.label}
                                    </MenuItem>
                                ))}
                            </TextField>
                        </Grid>
                        <Grid item>
                            <TextField label="Phone" value={phoneNumber} variant="standard" onChange={(e) => setPhoneNumber(e.target.value)} />
                        </Grid>
                        
                    </Grid>
                </Grid>
            </Grid>
            <TableContainer component={Paper}  className="form-admin-page">
                <Table aria-label="collapsible table" md={12} sm={12} lg={12} xs={12}
                    sx={{
                        "& .MuiTableRow-root:hover": {
                        backgroundColor: "whiteSmoke",
                        }
                    }}>
                    <TableHead>
                    <TableRow style={{backgroundColor:"whiteSmoke"}}>
                        {/* <TableCell/> */}
                        <TableCell align="left" style={{color:"#d1c286"}}><b>Order code</b></TableCell>
                        <TableCell align="left" style={{color:"#d1c286"}}><b>Total</b></TableCell>
                        <TableCell align="left" style={{color:"#d1c286"}}><b>Address</b></TableCell>
                        <TableCell align="left" style={{color:"#d1c286"}}><b>Phone</b></TableCell>
                        <TableCell align="left" style={{color:"#d1c286"}}><b>Status</b></TableCell>
                        <TableCell align="left" style={{color:"#d1c286"}}><b>Order Created</b></TableCell>
                        <TableCell align="left" style={{color:"#d1c286"}}><b>Action</b></TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                        {orders.map((value, index)=>{
                            return (
                                <Row
                                number = {index}
                                key={index}
                                value={value} row={value}/>)
                        })}
                    </TableBody>
                </Table>
                <Grid item md={12} sm={12} lg={12} xs={12} mt={5} mb={5}
                        container
                        direction="row"
                        justifyContent="center"
                        alignItems="center">
                    <Pagination count={noPage} defaultPage={currentPage} props={{color:"#d1c286"}} onChange={onChangePagination}/>
                </Grid>
            </TableContainer>
            <EditStatus/>
            <DeleteModal/>
        </>
    )
}
function Row({value}) {
    const [open, setOpen] = React.useState(false);
    const [customer , getDetailCustomer] = React.useState("");
    const [total, getTotal] = React.useState("");
    const [order, getOrder] = React.useState([])
    const { orderCode, statusEdit, statusOrder, orderStatus, phoneSearch, date, toast, account} = useSelector((reduxData)=>
            reduxData.accountReducer
    )
    
    const dispatch = useDispatch();
    React.useEffect(()=>{
       const api = async () => {
        var requestOptions = {
            method: 'GET',
            headers:{"Authorization": `Bearer ${account.accessToken}`},
            redirect: 'follow'
          };

        //lấy thông tin orders bằng id
        await fetchApi(process.env.REACT_APP_API_SHOP_24H + "/api" + "/orders/" + `${value._id}`, requestOptions)
            .then((data)=>{
                getOrder(data.data.Orders);
                const getPrice =  data.data.Orders;
                getTotal(getPrice.reduce((sumary, item)=> sumary+ item.Product.PromotionPrice*item.quantity, 0));
            })
            .catch((error) => {
                console.log( error.message)
            });

        //lấy thông tin order của từng khách hàng
        await fetchApi(process.env.REACT_APP_API_SHOP_24H + "/api" + "/ordersCustomer/" +`${value._id}`, requestOptions)
            .then((data)=>{
                getDetailCustomer(data.data)
            })
            .catch((error) => {
                console.log( error.message)
            });
        }
        api()
    },[ orderCode, statusEdit, statusOrder, orderStatus, phoneSearch,date, value, toast ])
    const onBtnEditClick = (value)=>{
        dispatch(setStatusEdit(true));
        var orderInfo = {
            orderId: value._id,
            customer: customer,
            total: total,
            status: value.Status
        };
        dispatch(getOrderInfo(orderInfo));
    }
    const onBtnDeleteClick =(value)=>{
        dispatch(deleteOrderHandler(value));
        dispatch(setStatusDelete(true));
    }
    return (
      <React.Fragment>
            <TableRow md={12} sm={12} lg={12} xs={12}>
                {/* <TableCell>
                    <IconButton
                    aria-label="expand row"
                    size="small"
                    onClick={() => setOpen(!open)}
                    >
                    {open ? <ExpandMore /> : <ExpandLess />}
                    </IconButton>
                </TableCell> */}
                <TableCell align="left">
                    <Link to={"/Administrator/Orders/"+ value._id}>
                        <Typography><b>{value._id.substr(value._id.length - 6)}</b></Typography>
                    </Link>
                </TableCell>
                <TableCell align="left">
                    <Typography><b>${total}</b></Typography>
                </TableCell>
                <TableCell align="left">
                    <Grid container direction="column">
                        {customer?
                        <Typography><b>{customer.Address.Provide},{customer.Address.District},{customer.Address.Ward},{customer.Address.Address}</b></Typography>
                        :<Typography>None</Typography>}
                    </Grid>
                </TableCell>
                <TableCell align="left">
                    <Grid container direction="column">
                        {customer?<Typography><b>{customer.Phone}</b></Typography>:null}
                    </Grid>
                </TableCell>
                <TableCell align="left">
                    <Typography><b>{value.Status}</b></Typography>
                </TableCell>
                <TableCell align="left">
                    {value.createdAt}
                </TableCell>

                <TableCell align="left" style={{paddingLeft: 0, paddingRight:0}}>
                    <Grid
                        container
                        direction="row"
                        justifyContent="center"
                        alignItems="center" >
                    <Button onClick={()=>onBtnEditClick(value)}> Edit</Button>
                    <Button color="error" onClick={()=>onBtnDeleteClick(value)}><DeleteIcon/></Button>
                    </Grid>
                </TableCell>
            </TableRow>
        <TableRow sx={{ '& > *': { borderBottom: 'unset' } }} md={12} sm={12} lg={12} xs={12}>
          <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <Box sx={{ margin: 1 }}>
                <Table size="small" aria-label="purchases">
                  <TableBody>
                        {order.map((orderDetail, index) =>{
                            return (
                                <TableRow key={index}>
                                    <TableCell><img src={orderDetail.Product.ImageUrl} style={{ width:"60px"}}/></TableCell>
                                    <TableCell><Typography style={{color:"#d1c286"}}><b>{orderDetail.Product.Name}</b></Typography></TableCell>
                                    <TableCell><Typography>Quantity: <b>{orderDetail.quantity}</b></Typography></TableCell>
                                    <TableCell><Typography>Available</Typography></TableCell>
                                </TableRow>
                            )
                        })}
                  </TableBody>
                </Table>
              </Box>
            </Collapse>
          </TableCell>
        </TableRow>
      </React.Fragment>
    );
  }

export default OrdersInfo;